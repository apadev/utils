import type { BuildConfig } from 'bun';
import dts from 'bun-plugin-dts';

const defaults: BuildConfig = {
  entrypoints: ['./src/index.ts', './src/match/index.ts', './src/bracket/index.ts', './src/scoresheet/index.ts'],
  plugins: [dts()],
  naming: '[dir]/[name].[ext]',
  outdir: './dist',
  // splitting: true,
  packages: 'external',
  // minify: true,
  target: 'browser',
  sourcemap: 'linked',
};

const commonjsConfig: BuildConfig = {
  ...defaults,
  format: 'cjs', // Generate CommonJS files
  naming: '[dir]/[name].cjs',
};

await Promise.all([
  Bun.build({ ...defaults, format: 'esm' }), // ESM output
  Bun.build(commonjsConfig), // CommonJS output
]);
