import fs from 'fs';
import { execSync } from 'child_process';
import semver from 'semver';

try {
  // Skip check if current branch is master
  const currentBranch = execSync('git rev-parse --abbrev-ref HEAD', { encoding: 'utf8' }).trim();
  if (currentBranch === 'master') process.exit(0);

  const packageJsonPath = 'package.json';
  if (!fs.existsSync(packageJsonPath)) {
    console.error('❌ package.json not found.');
    process.exit(1);
  }

  // Read current package.json version
  const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));
  const currentVersion = packageJson.version;

  // Read package.json version from master branch
  const masterPackageJsonRaw = execSync('git show origin/master:package.json', {
    encoding: 'utf8',
  });
  const masterPackageJson = JSON.parse(masterPackageJsonRaw);
  const masterVersion = masterPackageJson.version;

  const masterCompare = semver.compare(currentVersion, masterVersion);

  console.log(`️️📦 Master branch version: ${masterVersion}`);
  console.log(`️️📦 Current branch version: ${currentVersion}`);

  if (masterCompare !== 1) {
    console.error(`\n❌ package.json version has not changed. Please bump the version before merging the PR.`);
    process.exit(1);
  }
} catch (error) {
  console.error('❌️ Error checking package.json version:', error);
  process.exit(1);
}
