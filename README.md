[![codecov](https://codecov.io/bb/apadev/utils/branch/master/graph/badge.svg?token=4F8MZOG70K)](https://codecov.io/bb/apadev/utils)

# Shared APA business logic repository

This TypeScript repo contains business logic that can be reused across multiple apps/projects. It contains proprietary information that shall not be made public and must reside in a private NPM repository. We have chosen AWS CodeArtifact to be our private repository for this library.

# Adding To A Project

## Connecting to the repo

To add this to a project you must set the your NPM uptream to our `utils` repository. This is most easily done using the AWS CLI.
Run this to connect your development environment to our repository.
`aws codeartifact login --tool npm --repository utils --domain apa --domain-owner 797101096349 --region us-west-2`

## Staying connected for updates

Everytime you install or update packages you'll need to ensure there's a fresh connection to the `utils` CodeArtifact repository. The simplest way to do this is by adding this script to `package.json`.

```json
"scripts": {
  "ca:login": "aws codeartifact login --tool npm --repository utils --domain apa --domain-owner 797101096349 --region us-west-2 --namespace @apa",
  ...
}
```

You'll need to rerun the `ca:login` script anytime you want to install or update packages.

## Installing the package

This part is pretty straightforward once you have the connection established and the scripts above added to your `package.json` file.

### bun

```bash
bun run ca:login
bun add @apa/utils -S
```

### pnpm

```bash
pnpm run ca:login
pnpm add @apa/utils -S
```

# Contributing

The rules for contributing to this shared library are pretty simple too.

- [ ] Make the changes required
- [ ] Add or update tests for the change you made (Very Important)
- [ ] Bump the package version appropriately (You can do this manually or use [npm version](https://docs.npmjs.com/cli/v11/commands/npm-version))
