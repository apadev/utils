import assert from 'assert';

export function bracketSize(participantCount: number, finishers = 1): number {
  assert(
    finishers <= participantCount / 2,
    `backets cannot have more more than 50% (${Math.floor(participantCount / 2)}) of participants win`
  );
  assert(finishers >= 1, 'brackets must have atleast 1 finisher');
  assert(participantCount >= 3, 'brackets must have atleast 3 participants');

  return 2 ** Math.ceil(Math.log2(participantCount / finishers)) * finishers;
}
