import assert from 'assert';
import { setupFirstRound } from './setupFirstRound';
import { Direction, createNextRound } from './createNextRound';
import { createLoserBracket } from './createLoserBracket';
import { calcDoubleEliminationRounds } from './calcDoubleEliminationRounds';
import { bracketSize } from './bracketSize';
import { splitSize } from './splitSize';
import { assignMatchesToFlips } from './assignMatchesToFlips';

export type MatchType = {
  id: number;
  round: number;
  home?: string;
  away?: string;
  loserMatch?: MatchType;
  winnerMatch?: MatchType;
  flip: boolean;
  split: boolean;
};

export type FormatType = 'SINGLE' | 'SINGLE_MODFIED' | 'DOUBLE';
export type FlipType = 'SINGLE' | 'TEAM';

export function createBracket(
  _size: number,
  finishers: number,
  split = false,
  formatType: FormatType = 'SINGLE',
  flip?: FlipType
) {
  assert(finishers > 0, 'you must include the number of finsihers when creating a bracket');
  const bracketsize = bracketSize(_size, finishers);
  const splitsize = splitSize(_size, bracketsize);

  const size = split ? splitsize : bracketsize;

  assert(!!size, 'bracket size cannot be determined!');

  const mini = size / finishers <= 4;
  const winnerFlipRound = mini ? 1 : 2;

  const matches = setupFirstRound(split ? (size === 3 ? 3 : (size / 3) * 2) : size);
  const doubleEliminationRounds = calcDoubleEliminationRounds(formatType, split);

  const flipRounds = doubleEliminationRounds - 1;

  const keepRunning = (i: number) =>
    (i == 0 && split) ||
    (i <= winnerFlipRound && !!doubleEliminationRounds) ||
    matches.filter((m) => m.round == i).length > finishers;

  for (let i = 0; keepRunning(i); i++) {
    // after all the elimination rounds are done, create the loser bracket before continuing
    if (i === flipRounds) {
      createLoserBracket(matches, flipRounds, split, mini);
    }
    const isFlipRound = i == winnerFlipRound && !!doubleEliminationRounds;
    createNextRound(matches, Direction.WINNER, isFlipRound, split && i == 0);
  }

  if (formatType !== 'SINGLE') {
    assert(!!flip, 'Flip type must be set for brackets that are not SINGLE');
    assignMatchesToFlips(matches, flip, finishers);
  }

  // insure everything is in order
  matches.sort((match) => match.id);

  return matches;
}
