import { Direction, createNextRound } from './createNextRound';
import { MatchType } from './generate';

export function createLoserBracket(matches: MatchType[], flips: number, split: boolean = false, mini: boolean = false) {
  const lastRound = () => matches.filter((match) => match.round === matches[matches.length - 1].round);
  const finishers = split || mini ? lastRound().length : Math.round(lastRound().length / 2);
  let countFlips = 0;

  for (let i = 0; matches.filter((m) => m.round == i * -1).length > finishers || countFlips !== flips; i++) {
    const isFlip = split ? i % 2 === 0 : i % 2 === 1;
    if (isFlip) ++countFlips;

    createNextRound(matches, Direction.LOSER, isFlip);
  }
}
