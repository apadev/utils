import { MatchType, FlipType } from './generate';

function getRounds(matches: MatchType[]) {
  return matches.reduce((accum: Array<number>, match: MatchType): Array<number> => {
    if (!accum.includes(match.round)) accum.push(match.round);
    return accum;
  }, []);
}

function reverseOrder(matches: MatchType[]) {
  return matches.sort((a, b) => b.id - a.id);
}

function offset(matches: MatchType[], amount: number) {
  const _matches = [...matches];

  for (const [i, match] of _matches.entries()) {
    const offsetIndex = (matches.length + amount + i) % matches.length;

    matches[offsetIndex] = match;
  }
}

export function assignMatchesToFlips(matches: MatchType[], type: FlipType, numberOfFinishers: number) {
  const flipMatches = matches.filter((match) => match.flip);
  const matches_missing_loser = matches.filter((match) => match.round > 0 && !match.loserMatch);
  const matches_missing_winner = matches.filter((match) => match.round < 0 && !match.winnerMatch);
  const loserFlips = flipMatches.filter((m) => m.round < 0);
  const winnerFlips = flipMatches.filter((m) => m.round > 0);

  for (const [j, round] of getRounds(loserFlips).entries()) {
    const matchesToFlipTo = loserFlips.filter((m) => m.round === round);
    const matchesToFlipFrom = matches_missing_loser.filter((m) => m.round === getRounds(matches_missing_loser)[j]);

    if (type === 'TEAM') {
      if (numberOfFinishers > 2) offset(matchesToFlipTo, matchesToFlipTo.length / numberOfFinishers);
      else reverseOrder(matchesToFlipTo);
    } else {
      reverseOrder(matchesToFlipTo);
      if (matchesToFlipTo.length > 2) offset(matchesToFlipTo, matchesToFlipTo.length / 2);
    }

    for (const [i, match] of matchesToFlipFrom.entries()) {
      match.loserMatch = matchesToFlipTo[i];
    }
  }

  for (const round of getRounds(winnerFlips).reverse()) {
    const matchesToFlipTo = winnerFlips.filter((m) => m.round === round);
    const matchesToFlipFrom = matches_missing_winner.filter(
      (m) => m.round === getRounds(matches_missing_winner)[getRounds(matches_missing_winner).length - 1]
    );

    if (type === 'TEAM') offset(matchesToFlipTo, matchesToFlipTo.length / numberOfFinishers);

    for (const [i, match] of matchesToFlipFrom.entries()) {
      match.winnerMatch = matchesToFlipTo[i];
    }
  }
}
