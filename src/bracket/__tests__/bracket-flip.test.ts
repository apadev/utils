import { MatchType, createBracket } from '../index';

function expectFlipsToBeValid(matches: MatchType[], map: number[][]) {
  for (const [from, to] of map) {
    const match = matches.find((m) => m.id + 1 === from);

    expect(match.loserMatch?.id + 1 || match.winnerMatch?.id + 1).toBe(to);
  }
}

test('expect a 24-1 SINGLE SINGLE_MODFIED bracket to generate a valid flips', () => {
  const map = [
    // first round
    [9, 24],
    [10, 23],
    [11, 22],
    [12, 21],
    [13, 28],
    [14, 27],
    [15, 26],
    [16, 25],
    // sencond round
    [17, 34],
    [18, 33],
    [19, 36],
    [20, 35],
    // loser to winner flip
    [33, 37],
    [34, 38],
    [35, 39],
    [36, 40],
  ];
  const matches = createBracket(24, 1, true, 'SINGLE_MODFIED', 'SINGLE');

  expectFlipsToBeValid(matches, map);
});

test('expect a 24-2 SINGLE SINGLE_MODFIED bracket to generate a valid flips', () => {
  const map = [
    // first round
    [9, 24],
    [10, 23],
    [11, 22],
    [12, 21],
    [13, 28],
    [14, 27],
    [15, 26],
    [16, 25],
    // sencond round
    [17, 34],
    [18, 33],
    [19, 36],
    [20, 35],
    // loser to winner flip
    [33, 37],
    [34, 38],
    [35, 39],
    [36, 40],
  ];
  const matches = createBracket(24, 2, true, 'SINGLE_MODFIED', 'SINGLE');

  expectFlipsToBeValid(matches, map);
});

test('expect a 16-1 TEAM SINGLE_MODFIED bracket to generate a valid flips', () => {
  const map = [
    [9, 20],
    [10, 19],
    [11, 18],
    [12, 17],
    [21, 25],
    [22, 26],
  ];
  const matches = createBracket(16, 1, false, 'SINGLE_MODFIED', 'TEAM');

  expectFlipsToBeValid(matches, map);
});

test('expect a 24-3 TEAM SINGLE_MODFIED bracket to generate a valid flips', () => {
  const map = [
    [13, 29],
    [14, 30],
    [15, 25],
    [16, 26],
    [17, 27],
    [18, 28],
    [31, 39],
    [32, 37],
    [33, 38],
  ];
  const matches = createBracket(24, 3, false, 'SINGLE_MODFIED', 'TEAM');

  expectFlipsToBeValid(matches, map);
});

test('expect a 48-6 TEAM SINGLE_MODFIED bracket to generate a valid flips', () => {
  const map = [
    [25, 59],
    [26, 60],
    [27, 49],
    [28, 50],
    [29, 51],
    [30, 52],
    [31, 53],
    [32, 54],
    [33, 55],
    [34, 56],
    [35, 57],
    [36, 58],
  ];
  const matches = createBracket(48, 6, false, 'SINGLE_MODFIED', 'TEAM');

  expectFlipsToBeValid(matches, map);
});
