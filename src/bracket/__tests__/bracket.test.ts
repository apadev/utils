import { createBracket } from '../index';

test('bracket generates matches', () => {
  const matches = createBracket(16, 1);
  const amountOfWinners = matches.filter((m) => m.winnerMatch == null).length;

  expect(amountOfWinners).toBe(1);
  expect(!!matches).toBeTruthy();
});

test('expect a 9-1 bracket to generate a 16-1 bracket', () => {
  const matches = createBracket(16, 1);
  const nine_team_matches = createBracket(9, 1);

  expect(nine_team_matches.length).toBe(matches.length);
});

test('expect both SINGLE_MODFIED first round winners match to flip to losers bracket', () => {
  const matches = createBracket(8, 1, false, 'SINGLE_MODFIED', 'SINGLE');
  const first_winner_round = matches.filter((m) => m.round === 1);
  const last_loser_round = matches.filter((m) => m.round === -3);

  expect(first_winner_round[0].loserMatch?.id).toBe(9);
  expect(first_winner_round[1].loserMatch?.id).toBe(8);
  expect(last_loser_round[0].winnerMatch?.id).toBe(12);
});

test('expect a 24-1 bracket to generate a valid bracket', () => {
  const matches = createBracket(24, 1, true);
  const singlemodfiedmatches = createBracket(24, 1, true, 'SINGLE_MODFIED', 'SINGLE');
  const firstround = matches.filter((m) => m.round == 0);

  expect(firstround.length).toBe(8);
  expect(matches.length).toBe(23);
  expect(singlemodfiedmatches.length).toBe(43);
});
