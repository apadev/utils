import { FormatType } from './generate';

export function calcDoubleEliminationRounds(format: FormatType, split: boolean) {
  if (format === 'SINGLE_MODFIED' && split) return 3;
  if (format === 'SINGLE_MODFIED') return 2;

  return 0;
}
