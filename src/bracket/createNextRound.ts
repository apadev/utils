import { nextId } from './nextId';
import { MatchType } from './generate';

export enum Direction {
  LOSER = -1,
  WINNER = 1,
}

export function createNextRound(
  matches: MatchType[],
  direction = Direction.WINNER,
  flip: boolean = false,
  split = false
) {
  const round = matches.reduce((acc, match) => Math.max(acc, match.round * direction), 0) * direction;

  const previousMatches = matches.filter((match) => match.round === round);

  for (const [i, match] of previousMatches.entries()) {
    if (flip || split) {
      const newMatch: MatchType = {
        id: nextId(matches),
        round: round + direction,
        flip,
        split,
      };
      if (round == 0 && direction == Direction.LOSER) matches[match.id].loserMatch = newMatch;
      else matches[match.id].winnerMatch = newMatch;

      matches.push(newMatch);
    } else if (i % 2 === 0) {
      const newMatch: MatchType = {
        id: nextId(matches),
        round: round + direction,
        flip,
        split,
      };
      if (round == 0 && direction == Direction.LOSER) {
        matches[match.id].loserMatch = newMatch;
        matches[match.id + 1].loserMatch = newMatch;
      } else {
        matches[match.id].winnerMatch = newMatch;
        matches[match.id + 1].winnerMatch = newMatch;
      }
      matches.push(newMatch);
    }
  }
}
