export function splitSize(teamCount: number, bracketSize: number): number | undefined {
  const size = bracketSize - bracketSize / 4;

  if (size >= teamCount) return size;
}
