import { MatchType } from './generate';

// utilitly function to generate the next id
export function nextId(matches: MatchType[]) {
  return matches[matches.length - 1].id + 1;
}
