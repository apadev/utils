import { MatchType } from './generate';

export function setupFirstRound(size: number) {
  let id = -1;
  const matches: MatchType[] = [];
  // Populate first round
  for (let i = 0; i < size / 2; i++) {
    const match: MatchType = {
      id: ++id,
      round: 0,
      flip: false,
      split: false,
    };
    matches.push(match);
  }
  return matches;
}
