export enum Side {
  'HOME' = 'home',
  'AWAY' = 'away',
}

export const playerOpponent = (side): [Side, Side] =>
  side === Side.AWAY ? [Side.AWAY, Side.HOME] : [Side.HOME, Side.AWAY];
