import { MPEDictionary } from '../match/nineball/matchPointsEarned';
import { randomIntFromInterval } from '../mock/random';

export const ballPointsByPointsEarned = (pte: number, sl: number) => {
  const breakpoints = MPEDictionary[sl];
  if (pte === 0) return randomIntFromInterval(0, breakpoints[0] - 1);
};
