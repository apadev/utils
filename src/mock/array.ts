import { randomIntFromInterval } from './random';

export const randomValue = (arr) => {
  if (!Array.isArray(arr)) {
    throw new Error('Variable is not an array in array.randomValue');
  }
  if (!arr.length) return null;
  const idx = randomIntFromInterval(0, arr.length - 1);
  return arr[idx];
};
