import { canCompleteByPoints, canCompleteEightByPoints, canCompleteNineByPoints } from '../index';
import { FormatTypeListId } from '../teamPoints/types';

import {
  pm51to49,
  fiftyWithThree,
  fiftyWithTwo,
  eightToOne,
  eightOneForfeit,
  eightToFour,
  eightToTwo,
  sevenToOne,
  sevenToTwo,
  sevenToTwoTakingAwayPoint,
  sevenToTwoHill,
  sixToFour,
  threeSix,
  threeThree,
  threeZero,
  tieAway,
  tieHome,
  homeUnreachable,
  awayUnreachable,
  tie_away_byGames,
  tie_home_byGames,
  sevenToTwoSetup,
} from './lib';

test('Can complete by points: 8-ball league play', () =>
  expect(canCompleteByPoints(eightScoresheet, eightToFour)).toBe(false));
test('Can complete by points: 8-ball playoff', () =>
  expect(canCompleteByPoints(eightPlayoffScoresheet, eightToFour)).toBe(true));

test('Can complete by points: 9-ball league play', () =>
  expect(canCompleteByPoints(nineScoresheet, pm51to49)).toBe(false));
test('Can complete by points: 9-ball playoff', () =>
  expect(canCompleteByPoints(ninePlayoffScoresheet, pm51to49)).toBe(true));

// 51% of points
test('Eight CBP 8-1', () => {
  expect(canCompleteEightByPoints(eightToOne, 5)).toBe(true);
});
test('Eight CBP 8-1 Forfeit match', () => {
  expect(canCompleteEightByPoints(eightOneForfeit, 5)).toBe(true);
});
test('Eight CBP 8-4', () => {
  expect(canCompleteEightByPoints(eightToFour, 5)).toBe(true);
});
test('Eight CBP 8-2', () => {
  expect(canCompleteEightByPoints(eightToTwo, 5)).toBe(true);
});

// // Not enough points remaining for one side to win
test('Eight CBP 7-1', () => {
  expect(canCompleteEightByPoints(sevenToOne, 5)).toBe(true);
});
test('Eight CBP 7-2', () => {
  expect(canCompleteEightByPoints(sevenToTwo, 5)).toBe(true);
});
test('Eight CBP 7-2 take away a point', () => {
  expect(canCompleteEightByPoints(sevenToTwoTakingAwayPoint, 5)).toBe(true);
});
test('Eight CBP 7-2 hill', () => {
  expect(canCompleteEightByPoints(sevenToTwoHill, 5)).toBe(true);
});

//Ties
test('Eight CBP 7-7 home wins', () => {
  expect(canCompleteEightByPoints(tieHome, 5)).toBe(true);
});
test('Eight CBP 7-7 away wins', () => {
  expect(canCompleteEightByPoints(tieAway, 5)).toBe(true);
});
test('Eight CBP 6-6 home wins', () => {
  expect(canCompleteEightByPoints(homeUnreachable, 5)).toBe(true);
});
test('Eight CBP 6-6 away wins', () => {
  expect(canCompleteEightByPoints(awayUnreachable, 5)).toBe(true);
});

test('Eight CBP 7-x away wins', () => {
  expect(canCompleteEightByPoints(tie_away_byGames, 5)).toBe(true);
});
test('Eight CBP 7-x home wins', () => {
  expect(canCompleteEightByPoints(tie_home_byGames, 5)).toBe(true);
});

test('Eight CBP 7-x 4th match in setup: no winner', () => {
  expect(canCompleteEightByPoints(sevenToTwoSetup, 5)).toBe(false);
});
// falsey scenerios
test('Eight CBP 6-4', () => expect(canCompleteEightByPoints(sixToFour, 5)).toBe(false));
test('Eight CBP 3-0', () => expect(canCompleteEightByPoints(threeZero, 5)).toBe(false));

test('Eight CBP 3-3', () => expect(canCompleteEightByPoints(threeThree, 5)).toBe(false));
test('Eight CBP 3-6', () => expect(canCompleteEightByPoints(threeSix, 5)).toBe(false));

test('Throw matches expected error', () => {
  expect(() =>
    canCompleteByPoints({ matchesExpected: null, isPlayoff: true, format: FormatTypeListId.EIGHT }, eightToOne)
  ).toThrow();
});
test('Throw format error', () => {
  expect(() =>
    canCompleteByPoints({ matchesExpected: 5, isPlayoff: true, format: FormatTypeListId.MASTERS }, eightToOne)
  ).toThrow();
});

test('Masters match', () => {
  expect(() => canCompleteByPoints({ matchesExpected: 5, isPlayoff: true, format: 204 }, eightToOne)).toThrow();
});

// Nine Ball
test('Nine CBP 51-49', () => {
  const can = canCompleteNineByPoints(pm51to49, 5);
  expect(can).toBe(true);
});

test('Nine CBP 50 with 3 wins', () => {
  const can = canCompleteNineByPoints(fiftyWithThree, 5);
  expect(can).toBe(true);
});
test('Nine CBP 50 with 2 wins', () => {
  const can = canCompleteNineByPoints(fiftyWithTwo, 5);
  expect(can).toBe(false);
});

const ninePlayoffScoresheet = {
  format: 203,
  isPlayoff: true,
  matchesExpected: 5,
};
const eightPlayoffScoresheet = {
  format: 202,
  isPlayoff: true,
  matchesExpected: 5,
};
const nineScoresheet = { format: 203, isPlayoff: false, matchesExpected: 5 };
const eightScoresheet = { format: 202, isPlayoff: false, matchesExpected: 5 };
