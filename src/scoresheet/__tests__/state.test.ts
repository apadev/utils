import { ScoresheetStates, state } from '../state/state';
import {
  inProgressMatchScoreSheet,
  completeMatchScoreSheet,
  completedByPointsScoreSheet,
  forefeitTeamScoreSheet,
  forefeitReaminingScoreSheet,
} from './lib/state/context';
test('Scoresheet State In Progress Matches', () => {
  const result = state(inProgressMatchScoreSheet);
  expect(result).toBe(ScoresheetStates.IN_PROGRESS);
});

test('Scoresheet State In Progress Matches', () => {
  const result = state(inProgressMatchScoreSheet);
  expect(result).toBe(ScoresheetStates.IN_PROGRESS);
});
test('Scoresheet State Complete Matches', () => {
  const result = state(completeMatchScoreSheet);
  expect(result).toBe(ScoresheetStates.COMPLETE);
});
test('Scoresheet State Forfiet Remaining', () => {
  const result = state(forefeitReaminingScoreSheet);
  expect(result).toBe(ScoresheetStates.FOREFEIT_REMAINING);
});
test('Scoresheet State Forfeit Team', () => {
  const result = state(forefeitTeamScoreSheet);
  expect(result).toBe(ScoresheetStates.FOREFEIT_TEAM);
});
test('Scoresheet State Complete By Points', () => {
  const result = state(completedByPointsScoreSheet);
  expect(result).toBe(ScoresheetStates.COMPLETE_BY_POINTS);
});
test('Null Scoresheet', () => {
  const result = () => state(null);
  expect(result).toThrow();
});
