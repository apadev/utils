import { PlayerMatch_CBP_Eight } from '../../../completeByPoints/eightBall';
export const tieHome: PlayerMatch_CBP_Eight[] = [
  {
    //2-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //3-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-3
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-3
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-3
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const tieAway: PlayerMatch_CBP_Eight[] = [
  {
    //0-2
    home: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-3
    home: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //3-0
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //3-0
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //1-2
    home: {
      raceTo: 3,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const tie_away_byGames: PlayerMatch_CBP_Eight[] = [
  {
    //1-2
    home: {
      raceTo: 4,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-3
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-2
    away: {
      raceTo: 5,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-1
    away: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 0-0
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const tie_home_byGames: PlayerMatch_CBP_Eight[] = [
  {
    //2-1
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //3-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-2
    home: {
      raceTo: 5,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-1
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 0-0
    home: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];
