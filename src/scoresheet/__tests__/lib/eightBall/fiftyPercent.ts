import { PlayerMatch_CBP_Eight } from '../../../completeByPoints/eightBall';
export const sevenToOne: PlayerMatch_CBP_Eight[] = [
  {
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const sevenToTwo: PlayerMatch_CBP_Eight[] = [
  {
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 3,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const sevenToTwoTakingAwayPoint: PlayerMatch_CBP_Eight[] = [
  {
    //3-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-1
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-1
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 0-0 but away can only get one
    home: {
      raceTo: 3,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const sevenToTwoHill: PlayerMatch_CBP_Eight[] = [
  {
    //3-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-1
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-1
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 1-0
    home: {
      raceTo: 2,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const homeUnreachable: PlayerMatch_CBP_Eight[] = [
  {
    //2-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-0
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-0
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 5,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 0-0
    home: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const awayUnreachable: PlayerMatch_CBP_Eight[] = [
  {
    //2-0
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-0
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //2-0
    away: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //0-0
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 5,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 0-0
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const sevenToTwoSetup: PlayerMatch_CBP_Eight[] = [
  {
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 0,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 0,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];
