import { PlayerMatch_CBP_Eight } from '../../../completeByPoints/eightBall';

export const eightOneForfeit: PlayerMatch_CBP_Eight[] = [
  {
    away: {
      __typename: 'PlayerMatch_Side_Eight',
      didForfeit: false,
      raceTo: 2,
      racksWon: 2,
    },

    home: {
      __typename: 'PlayerMatch_Side_Eight',
      didForfeit: false,
      raceTo: 3,
      racksWon: 0,
    },
  },
  {
    away: {
      __typename: 'PlayerMatch_Side_Eight',
      didForfeit: false,
      raceTo: 0,
      racksWon: 0,
    },
    home: {
      __typename: 'PlayerMatch_Side_Eight',
      didForfeit: true,
      raceTo: 0,
      racksWon: 0,
    },
  },
  {
    away: {
      __typename: 'PlayerMatch_Side_Eight',
      didForfeit: false,
      raceTo: 2,
      racksWon: 2,
    },
    home: {
      __typename: 'PlayerMatch_Side_Eight',
      didForfeit: false,
      raceTo: 4,
      racksWon: 3,
    },
  },
];
export const eightToOne: PlayerMatch_CBP_Eight[] = [
  {
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 5,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 4,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const eightToTwo: PlayerMatch_CBP_Eight[] = [
  {
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 4,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const eightToFour: PlayerMatch_CBP_Eight[] = [
  {
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 4,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    home: {
      raceTo: 5,
      racksWon: 4,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 4,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];
