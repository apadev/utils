import { PlayerMatch_CBP_Eight } from '../../../completeByPoints/eightBall';
export const sixToFour: PlayerMatch_CBP_Eight[] = [
  {
    // 3-0
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //6-0
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //6-2
    home: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //6-4
    home: {
      raceTo: 4,
      racksWon: 1,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 3,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    //6-4
    home: {
      raceTo: 2,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const threeZero: PlayerMatch_CBP_Eight[] = [
  {
    // 3-0
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];

export const threeThree: PlayerMatch_CBP_Eight[] = [
  {
    // 3-0
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 3-3
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];
export const threeSix: PlayerMatch_CBP_Eight[] = [
  {
    // 3-0
    home: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 3-3
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
  {
    // 3-6
    home: {
      raceTo: 3,
      racksWon: 0,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
    away: {
      raceTo: 2,
      racksWon: 2,
      didForfeit: false,
      __typename: 'PlayerMatch_Side_Eight',
    },
  },
];
