import { PlayerMatch_CBP_Nine } from '../../../completeByPoints/nineBall';
export const fiftyWithThree: PlayerMatch_CBP_Nine[] = [
  // 12-8
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 24-16
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 36-24
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 44-36
  {
    home: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 51-49
  {
    home: {
      ballPoints: 10,
      raceTo: 14,
      skillLevel: 1,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 25,
      raceTo: 38,
      skillLevel: 5,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
];

export const fiftyWithTwo: PlayerMatch_CBP_Nine[] = [
  // 12-8 Home
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 24-16 home
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 32-28 away
  {
    away: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    home: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 34-46
  {
    home: {
      ballPoints: 9,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 34-50
  {
    home: {
      ballPoints: 2,
      raceTo: 14,
      skillLevel: 1,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 19,
      raceTo: 38,
      skillLevel: 5,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
];
