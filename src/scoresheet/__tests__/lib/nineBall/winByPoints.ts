import { PlayerMatch_CBP_Nine } from '../../../completeByPoints/nineBall';

export const pm51to49: PlayerMatch_CBP_Nine[] = [
  // 12-8
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 24-16
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 36-24
  {
    home: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 44-36
  {
    home: {
      ballPoints: 30,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 31,
      raceTo: 31,
      skillLevel: 4,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
  // 51-49
  {
    home: {
      ballPoints: 11,
      raceTo: 14,
      skillLevel: 1,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
    away: {
      ballPoints: 38,
      raceTo: 38,
      skillLevel: 5,
      __typename: 'PlayerMatch_Side_Nine',
      didForfeit: false,
    },
  },
];
