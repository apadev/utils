import { ScoreSheetState_State, PlayerMatch_State } from '../../../state/context';
export const completedMatches: Array<PlayerMatch_State> = [
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
];
export const noMatches: Array<PlayerMatch_State> = [];

export const inProgressMatches: Array<PlayerMatch_State> = [
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: new Date('2020-8-27- 09:33:17.001'),
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: true,
    endTime: null,
    __typename: 'PlayerMatch_State',
  },
];
export const noMatchStarted: Array<PlayerMatch_State> = [
  {
    playStarted: false,
    endTime: null,
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: false,
    endTime: null,
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: false,
    endTime: null,
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: false,
    endTime: null,
    __typename: 'PlayerMatch_State',
  },
  {
    playStarted: false,
    endTime: null,
    __typename: 'PlayerMatch_State',
  },
];
export const scoreSheetWithNoMatches: ScoreSheetState_State = {
  completedByPoints: false,
  forfeitBy: null,
  matchesExpected: 5,
  playerMatches: noMatches,
  __typename: `ScoreSheetState_State`,
};

export const scoreSheetWithNullMatches: ScoreSheetState_State = {
  completedByPoints: false,
  forfeitBy: null,
  matchesExpected: 5,
  playerMatches: null,
  __typename: `ScoreSheetState_State`,
};

export const inProgressMatchScoreSheet: ScoreSheetState_State = {
  completedByPoints: false,
  forfeitBy: null,
  matchesExpected: 5,
  playerMatches: inProgressMatches,
  __typename: `ScoreSheetState_State`,
};

export const completeMatchScoreSheet: ScoreSheetState_State = {
  completedByPoints: false,
  forfeitBy: null,
  matchesExpected: 5,
  playerMatches: completedMatches,
  __typename: `ScoreSheetState_State`,
};

export const nullCompletedByPointsScoreSheet: ScoreSheetState_State = {
  completedByPoints: null,
  forfeitBy: null,
  matchesExpected: 5,
  playerMatches: completedMatches,
  __typename: `ScoreSheetState_State`,
};
export const forefeitReaminingScoreSheet: ScoreSheetState_State = {
  completedByPoints: null,
  forfeitBy: 'Away',
  matchesExpected: 5,
  playerMatches: inProgressMatches,
  __typename: `ScoreSheetState_State`,
};
export const forefeitTeamScoreSheet: ScoreSheetState_State = {
  completedByPoints: null,
  forfeitBy: 'home',
  matchesExpected: 5,
  playerMatches: noMatchStarted,
  __typename: `ScoreSheetState_State`,
};
export const completedByPointsScoreSheet: ScoreSheetState_State = {
  completedByPoints: true,
  forfeitBy: null,
  matchesExpected: 5,
  playerMatches: inProgressMatches,
  __typename: `ScoreSheetState_State`,
};
