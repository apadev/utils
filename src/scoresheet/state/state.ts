import { ScoreSheetState_State } from './context';

export const ScoresheetStates = {
  COMPLETE_BY_POINTS: 'COMPLETE_BY_POINTS',
  FOREFEIT_REMAINING: 'FOREFEIT_REMAINING',
  FOREFEIT_TEAM: 'FOREFEIT_TEAM',
  COMPLETE: 'COMPLETE',
  IN_PROGRESS: 'IN_PROGRESS',
};

export const state = (scoreSheet: ScoreSheetState_State) => {
  if (!scoreSheet) {
    throw new Error(`No scoresheet provided`);
  }
  const { completedByPoints, forfeitBy, matchesExpected, playerMatches } = scoreSheet;
  if (completedByPoints) return ScoresheetStates.COMPLETE_BY_POINTS;
  if (forfeitBy) {
    if (playerMatches.some((pm) => pm.playStarted)) {
      return ScoresheetStates.FOREFEIT_REMAINING;
    }
    return ScoresheetStates.FOREFEIT_TEAM;
  }
  const completeMatches = playerMatches.filter((pm) => pm.endTime);
  if (completeMatches.length === matchesExpected) {
    return ScoresheetStates.COMPLETE;
  }
  return ScoresheetStates.IN_PROGRESS;
};
