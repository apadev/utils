export interface PlayerMatch_State {
  playStarted: boolean;
  endTime: Date;
  __typename: `PlayerMatch_State`;
}
export interface ScoreSheetState_State {
  completedByPoints: boolean;
  forfeitBy: string;
  matchesExpected: number;
  playerMatches: Array<PlayerMatch_State>;
  __typename: `ScoreSheetState_State`;
}

export interface TestNumbers {
  numberOne: number;
  numberTwo: number;
  __typename: 'TestNumbers';
}
