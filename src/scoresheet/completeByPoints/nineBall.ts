import { matchPointsEarned } from '../../match/nineball/matchPointsEarned';

import { PlayerMatch_Side_Nine } from '../../match/state';

export type PlayerMatch_CBP_Nine = {
  home: PlayerMatch_Side_Nine;
  away: PlayerMatch_Side_Nine;
};

interface Cumulative {
  home: number;
  away: number;
  homeWins: number;
  awayWins: number;
}

export const canCompleteNineByPoints = (playerMatches: Array<PlayerMatch_CBP_Nine>, matchesExpected: number) => {
  const halfMatchPoints = matchesExpected * 10;
  const halfWins = Math.ceil(matchesExpected / 2);
  const cumulative: Cumulative = playerMatches.reduce(
    (acc, { home, away }) => {
      if (!home || !away) return acc;
      const add =
        home.didForfeit || away.didForfeit
          ? {
              home: home.didForfeit ? 0 : 20,
              away: away.didForfeit ? 0 : 20,
              homeWins: home.didForfeit ? 0 : 1,
              awayWins: away.didForfeit ? 0 : 1,
            }
          : {
              home: matchPointsEarned(home.skillLevel, home.ballPoints),
              away: matchPointsEarned(away.skillLevel, away.ballPoints),
              homeWins: home.raceTo <= home.ballPoints ? 1 : 0,
              awayWins: away.raceTo <= away.ballPoints ? 1 : 0,
            };

      // if one side has won we need to adjust their total
      if (home.raceTo <= home.ballPoints) add.home = 20 - add.away;
      if (away.raceTo <= away.ballPoints) add.away = 20 - add.home;

      const retObj = { home: 0, away: 0, homeWins: 0, awayWins: 0 };
      Object.keys(acc).map((key) => {
        retObj[key] = acc[key] + add[key];
      });
      return retObj;
    },
    {
      home: 0,
      away: 0,
      homeWins: 0,
      awayWins: 0,
    }
  );

  if (cumulative.home > halfMatchPoints || cumulative.away > halfMatchPoints) return true;

  // if someone is at 50 points and has more than half the wins
  if (
    (cumulative.home === halfMatchPoints && cumulative.homeWins >= halfWins) ||
    (cumulative.away === halfMatchPoints && cumulative.awayWins >= halfWins)
  )
    return true;

  return false;
};
