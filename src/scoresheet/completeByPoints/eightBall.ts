import { eightBallPoints } from '../../match/eightBall';
import { PlayerMatch_Side_Eight } from '../../match/state';

export type PlayerMatch_CBP_Eight = {
  home: PlayerMatch_Side_Eight;
  away: PlayerMatch_Side_Eight;
  isSuddenDeath?: boolean;
};

interface Cumulative {
  home: number;
  away: number;
  homeWins: number;
  awayWins: number;
}

export const canCompleteEightByPoints = (playerMatches: Array<PlayerMatch_CBP_Eight>, matchesExpected: number) => {
  const pointsToWin = matchesExpected * 3 - Math.floor(matchesExpected / 2) * 3 - 1;
  const matchesToWin = Math.ceil(matchesExpected / 2);

  const cumulative = playerMatches.reduce(
    (acc, { home, away, isSuddenDeath }): Cumulative => {
      if (!home || !away) return acc;
      const add = {
        home: eightBallPoints(home, away, isSuddenDeath),
        away: eightBallPoints(away, home, isSuddenDeath),
        homeWins: eightBallPoints(home, away, isSuddenDeath) > 1 ? 1 : 0,
        awayWins: eightBallPoints(away, home, isSuddenDeath) > 1 ? 1 : 0,
      };
      const retObj = {};
      Object.keys(acc).map((key) => {
        retObj[key] = acc[key] + add[key];
      });
      return retObj as Cumulative;
    },
    {
      home: 0,
      away: 0,
      homeWins: 0,
      awayWins: 0,
    }
  );
  if (cumulative.home >= pointsToWin || cumulative.away >= pointsToWin) return true;
  const emptyPmPoints = (matchesExpected - playerMatches.length) * 3;

  const pointsRemainingToWin = playerMatches.reduce(
    (acc, { home, away }) => {
      return {
        home: acc.home + pointsLeftInMatch(home, away),
        away: acc.away + pointsLeftInMatch(away, home),
      };
    },
    {
      home: emptyPmPoints > 0 ? emptyPmPoints : 0,
      away: emptyPmPoints > 0 ? emptyPmPoints : 0,
    }
  );

  // one side can win all the points and still not win
  if (cumulative.home + pointsRemainingToWin.home < cumulative.away) {
    return true;
  }
  if (cumulative.away + pointsRemainingToWin.away < cumulative.home) {
    return true;
  }

  // if home wins all the points left.
  if (cumulative.home + pointsRemainingToWin.home === cumulative.away) {
    //there would be a tie and it would go to away
    if (cumulative.awayWins >= matchesToWin) return true;
  }

  // if away wins all the points left.
  if (cumulative.away + pointsRemainingToWin.away === cumulative.home) {
    //there would be a tie and it would go to home
    if (cumulative.homeWins >= matchesToWin) return true;
  }

  return false;
};

const pointsLeftInMatch = (player, opponent) => {
  if (!player || !opponent || !player.raceTo || !opponent.raceTo) return 3;
  // if match is over
  if (player.raceTo <= player.racksWon || opponent.raceTo <= opponent.racksWon) return 0;

  // if opponent has "taken away a point"
  if (opponent.racksWon > 0) {
    return player.racksWon === player.raceTo - 1 ? 1 : 2;
  }

  return player.racksWon === player.raceTo - 1 ? 2 : 3;
};
