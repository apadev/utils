import { canCompleteNineByPoints, PlayerMatch_CBP_Nine } from './nineBall';
import { canCompleteEightByPoints, PlayerMatch_CBP_Eight } from './eightBall';
import { PlayerMatch_Side_Eight, PlayerMatch_Side_Nine } from '../../match/state';
import { FormatTypeListId } from 'scoresheet/teamPoints/types';

const missingFields = (obj, depKeys) => {
  const missing = [];
  depKeys.forEach((key) => {
    if (obj[key] === undefined) missing.push(key);
  });
  if (!missing.length) return;
  return missing;
};

export const containsEightRequiredFields = (
  playerMatch: PlayerMatch_Side_Nine | PlayerMatch_Side_Eight
): playerMatch is PlayerMatch_Side_Eight => {
  if (
    [
      (playerMatch as PlayerMatch_Side_Eight).didForfeit,
      (playerMatch as PlayerMatch_Side_Eight).raceTo,
      (playerMatch as PlayerMatch_Side_Eight).racksWon,
    ].includes(undefined)
  ) {
    const keys = missingFields(playerMatch, ['didForfeit', 'raceTo', 'racksWon']);
    throw new Error(`Player match is missing input value${keys.length > 1 ? 's' : ''} ${keys} for utils/CBP`);
  }
  return true;
};

export const containsNineRequiredFields = (
  playerMatch: PlayerMatch_Side_Nine | PlayerMatch_Side_Eight
): playerMatch is PlayerMatch_Side_Nine => {
  if (
    [
      (playerMatch as PlayerMatch_Side_Nine).didForfeit,
      (playerMatch as PlayerMatch_Side_Nine).raceTo,
      (playerMatch as PlayerMatch_Side_Nine).ballPoints,
      (playerMatch as PlayerMatch_Side_Nine).skillLevel,
    ].includes(undefined)
  ) {
    const keys = missingFields(playerMatch, ['didForfeit', 'raceTo', 'ballPoints', 'skillLevel']);
    throw new Error(`Player match is missing input value${keys.length > 1 ? 's' : ''} ${keys} for utils/CBP`);
  }
  return true;
};

export interface CBP_Options {
  isPlayoff: boolean;
  matchesExpected: number;
  format: FormatTypeListId;
  isTournamentFormat?: boolean;
}

export const canCompleteByPoints = (
  options: CBP_Options,
  playerMatches: Array<PlayerMatch_CBP_Eight | PlayerMatch_CBP_Nine>
) => {
  const { isPlayoff, matchesExpected, format, isTournamentFormat } = options;

  if (!isTournamentFormat && !isPlayoff) return false;

  if (!matchesExpected) {
    throw new Error('matches expected is missing in can complete by points');
  }
  if (!format) throw new Error('format is missing in can complete by points');

  if (format === 202) {
    const eightPlayerMatches = playerMatches
      .filter((p) => p.home && p.away) // filters matches in setup
      .map((pm) => {
        containsEightRequiredFields(pm.home);
        containsEightRequiredFields(pm.away);
        return pm as PlayerMatch_CBP_Eight;
      });

    return canCompleteEightByPoints(eightPlayerMatches, matchesExpected);
  }

  if (format === 203) {
    const ninePlayerMatches = playerMatches
      .filter((p) => p.home && p.away) // filters matches in setup
      .map((pm) => {
        containsNineRequiredFields(pm.home);
        containsNineRequiredFields(pm.away);
        return pm as PlayerMatch_CBP_Nine;
      });
    return canCompleteNineByPoints(ninePlayerMatches, matchesExpected);
  }
  throw new Error('Masters is not implemented in can complete by points');
};

export * from './eightBall';
export * from './nineBall';
//matchesExpected, playerMatches
