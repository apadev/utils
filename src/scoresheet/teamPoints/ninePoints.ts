import { Side, playerOpponent } from '../../lib/side';
import { TeamPoints_PlayerMatch } from './types';
import { matchPointsEarned } from '../../match/nineball/matchPointsEarned';

export const ninePoints = (playerMatch: TeamPoints_PlayerMatch, side: Side) => {
  const ballPoints = playerMatch.racks.reduce(
    (acc, rack) => {
      if (rack.type !== 'NINE_RACK') return acc;
      return { home: acc.home + rack.home.ballPoints, away: acc.away + rack.away.ballPoints };
    },
    {
      home: 0,
      away: 0,
    }
  );

  const [player, opponent] = playerOpponent(side);
  if (!playerMatch[player].raceTo || !playerMatch[opponent].raceTo) return 0;

  if (playerMatch[player].didForfeit || playerMatch[opponent].didForfeit) {
    return playerMatch[player].didForfeit ? 0 : playerMatch.forfeitPoints;
  }
  //if match won by player
  if (playerMatch[player].raceTo <= ballPoints[player]) {
    return 20 - matchPointsEarned(playerMatch[opponent].skillLevel, ballPoints[opponent]);
  }
  return matchPointsEarned(playerMatch[player].skillLevel, ballPoints[player]);
};
