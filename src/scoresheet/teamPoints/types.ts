import { Side } from '../../lib/side';

export interface TeamPoints_PlayerMatch {
  home: PlayerMatch_Side;
  away: PlayerMatch_Side;
  racks: Array<TeamPoints_NineRack | TeamPoints_EightRack>;
  playStarted: boolean;
  forfeitPoints: number;
}
export interface PlayerMatch_Side {
  didForfeit: boolean;
  raceTo: number;
  skillLevel: number;
}

export interface TeamPoints_NineRack {
  type: 'NINE_RACK';
  home: TeamPoints_NineRack_Side;
  away: TeamPoints_NineRack_Side;
}
export interface TeamPoints_EightRack {
  type: 'EIGHT_RACK';
  home: TeamPoints_EightRack_Side;
  away: TeamPoints_EightRack_Side;
}

export interface TeamPoints_NineRack_Side {
  ballPoints: number;
}
export interface TeamPoints_EightRack_Side {
  wonRack: boolean;
}

export enum FormatType {
  'EIGHT' = 'EIGHT',
  'NINE' = 'NINE',
}
export enum FormatTypeListId {
  'EIGHT' = 202,
  'NINE' = 203,
  'MASTERS' = 204,
}

export interface TeamPoint_TeamMatch {
  type: FormatType;
  forfeitBy: Side;
  matchesExpected: number;
}
