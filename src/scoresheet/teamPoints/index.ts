import { eightPoints } from './eightPoints';
import { ninePoints } from './ninePoints';
import { TeamPoints_PlayerMatch, TeamPoint_TeamMatch, FormatType } from './types';
import { Side, playerOpponent } from '../../lib/side';

export interface TeamPointsInput {
  playerMatches: Array<TeamPoints_PlayerMatch>;
  teamFFP: number;
  matchFFP: number;
  side: Side;
  teamMatch: TeamPoint_TeamMatch;
}

export const teamPoints = ({ playerMatches, teamFFP, matchFFP, teamMatch, side }: TeamPointsInput) => {
  if (!playerMatches || !teamFFP || !matchFFP || !teamMatch || !side)
    throw new Error('team points is missing information');
  // if the only matches are forfeited before play starts we ignore player match points
  if (teamMatch.forfeitBy && !playerMatches.find((pm) => pm && pm.playStarted)) {
    return side === teamMatch.forfeitBy ? 0 : teamFFP;
  }

  // Not a full team match forfeit so we calculate based on individual player matches
  const pointsPerMatch = playerMatches.map((pm) => {
    if (!pm.home || !pm.away) return 0;
    const [player, opponent] = playerOpponent(side);
    //TODO: forfeit tests
    if (pm[player].didForfeit) return 0;
    if (pm[opponent].didForfeit) return matchFFP;
    if (!pm.playStarted) return 0;
    return teamMatch.type === FormatType.EIGHT
      ? eightPoints(pm, side)
      : teamMatch.type === FormatType.NINE
        ? ninePoints(pm, side)
        : 0;
  }) as number[];

  const missingPoints = (() => {
    if (!teamMatch.forfeitBy) return 0;
    if (side === teamMatch.forfeitBy) return 0;
    const numStarted = playerMatches.filter((pm) => pm && pm.playStarted).length;
    let numMissing = teamMatch.matchesExpected - numStarted;
    if (numMissing < 1) numMissing = 0;

    return numMissing * matchFFP;
  })();
  //TODO: Tests for teamPoints
  return pointsPerMatch.reduce((acc, points) => points + acc, missingPoints);
};
