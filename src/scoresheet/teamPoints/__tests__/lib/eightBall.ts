import { TeamPoints_PlayerMatch, TeamPoints_EightRack } from '../../types';
import { Side } from '../../../../lib/side';

class Rack implements TeamPoints_EightRack {
  away;
  home;
  type;
  constructor(winner?: Side) {
    this.home = { wonRack: false };
    this.away = { wonRack: false };
    if (winner) this[winner].wonRack = true;
    this.type = 'EIGHT_RACK';
  }
}

class PlayerMatch implements TeamPoints_PlayerMatch {
  away = { raceTo: 0, didForfeit: false, skillLevel: 0 };
  home = { raceTo: 0, didForfeit: false, skillLevel: 0 };
  playStarted = false;
  racks = [];
  forfeitPoints = 0;
  constructor(homeRaceTo, awayRaceTo, forfeitPoints) {
    if (homeRaceTo) this.home.raceTo = homeRaceTo;
    if (awayRaceTo) this.away.raceTo = awayRaceTo;
    if (forfeitPoints) this.forfeitPoints = forfeitPoints;
  }
  setDidForfeit = (side: Side) => {
    this[side].didForfeit = true;
  };
  setRacks = (racks: Array<Rack>) => {
    this.racks = racks;
  };
}

interface GetPlayerMatchInput_Side {
  raceTo: number;
  wins: number;
  forfeit?: boolean;
}

export const getPlayerMatch = (
  [home, away]: [GetPlayerMatchInput_Side?, GetPlayerMatchInput_Side?] = [],
  ffp: number,
  rackInProgress?: boolean
) => {
  if (!home || !away) return new PlayerMatch(0, 0, ffp);
  const playerMatch = new PlayerMatch(home.raceTo, away.raceTo, ffp);
  if (home.forfeit) playerMatch.setDidForfeit(Side.HOME);
  if (away.forfeit) playerMatch.setDidForfeit(Side.AWAY);
  const homeWinRacks = Array.from(Array(home.wins).keys()).map(() => new Rack(Side.HOME));
  const awayWinRacks = Array.from(Array(away.wins).keys()).map(() => new Rack(Side.AWAY));
  const racks = [...homeWinRacks, ...awayWinRacks];
  if (rackInProgress) racks.push(new Rack());
  playerMatch.setRacks(racks);
  return playerMatch;
};
