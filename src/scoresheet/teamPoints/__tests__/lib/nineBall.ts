import { TeamPoints_PlayerMatch, TeamPoints_NineRack } from '../../types';
import { Side } from '../../../../lib/side';

class Rack implements TeamPoints_NineRack {
  away = { ballPoints: 0 };
  home = { ballPoints: 0 };
  type;
  constructor(homeBp: number, awayBp: number) {
    this.home = { ballPoints: homeBp };
    this.away = { ballPoints: awayBp };
    this.type = 'NINE_RACK';
  }
}

class PlayerMatch implements TeamPoints_PlayerMatch {
  away = { raceTo: 0, didForfeit: false, skillLevel: 0 };
  home = { raceTo: 0, didForfeit: false, skillLevel: 0 };
  playStarted = false;
  racks = [];
  forfeitPoints = 0;
  constructor(home, away, forfeitPoints) {
    if (home) this.home = home;
    if (away) this.away = away;
    if (forfeitPoints) this.forfeitPoints = forfeitPoints;
  }
  setDidForfeit = (side: Side) => {
    this[side].didForfeit = true;
    return this;
  };
  setRacks = (racks: Array<Rack>) => {
    this.racks = racks;
    return this;
  };
  setSL = (side: Side, sl: number) => {
    this[side].skillLevel = sl;
    return this;
  };
  setPlayStarted = (bool) => {
    this.playStarted = bool;
    return this;
  };
}

interface GetPlayerMatchInput_Side {
  raceTo: number;
  ballPoints: number;
  forfeit?: boolean;
  skillLevel: number;
}

const rand = (max) => Math.ceil(Math.random() * max);

export const getPlayerMatch = (
  [home, away]: [GetPlayerMatchInput_Side?, GetPlayerMatchInput_Side?] = [],
  forfeitPoints: number = 15
) => {
  if (!home || !away) return new PlayerMatch(0, 0, forfeitPoints);
  const playerMatch = new PlayerMatch(home, away, forfeitPoints);
  if (home.forfeit) playerMatch.setDidForfeit(Side.HOME);
  if (away.forfeit) playerMatch.setDidForfeit(Side.AWAY);
  const racks = [];
  let homePointWell = home.ballPoints * 1;
  let awayPointWell = away.ballPoints * 1;
  while (homePointWell > 0 || awayPointWell > 0) {
    const hp = rand(homePointWell >= 10 ? 10 : homePointWell);
    const oppositePoints = 10 - hp;
    const ap = awayPointWell > oppositePoints ? oppositePoints : awayPointWell;
    racks.push(new Rack(hp, ap));
    homePointWell = homePointWell - hp;
    awayPointWell = awayPointWell - ap;
  }

  playerMatch.setRacks(racks);
  return playerMatch;
};
