import { ninePoints } from '../ninePoints';
import { getPlayerMatch } from './lib/nineBall';
import { Side } from '../../../lib/side';
import nineBallPoints, { skillLevelsAvailable } from '../../../match/nineBallPoints';

const ninePointsSide = (playerMatch) => [ninePoints(playerMatch, Side.HOME), ninePoints(playerMatch, Side.AWAY)];

test('ninePoints with setup match', () => {
  const result = ninePoints(getPlayerMatch(), Side.AWAY);
  expect(result).toBe(0);
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`nine home sweep ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl },
          { raceTo: nineBallPoints(oppSl), ballPoints: 0, skillLevel: oppSl },
        ])
      );
      expect(home).toBe(20);
      expect(away).toBe(0);
    });
    test(`nine away sweep ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(oppSl), ballPoints: 0, skillLevel: oppSl },
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl },
        ])
      );
      expect(home).toBe(0);
      expect(away).toBe(20);
    });
  });
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`nine home on the hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl) - 1, skillLevel: sl },
          { raceTo: nineBallPoints(oppSl), ballPoints: 0, skillLevel: oppSl },
        ])
      );
      expect(home).toBe(8);
      expect(away).toBe(0);
    });
    test(`nine away on the hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(oppSl), ballPoints: 0, skillLevel: oppSl },
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl) - 1, skillLevel: sl },
        ])
      );
      expect(home).toBe(0);
      expect(away).toBe(8);
    });
  });
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`nine hill-hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl) - 1, skillLevel: sl },
          { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl },
        ])
      );
      expect(home).toBe(8);
      expect(away).toBe(8);
    });
  });
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`nine home win vs hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl },
          { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl },
        ])
      );
      expect(home).toBe(12);
      expect(away).toBe(8);
    });
    test(`nine away win vs hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = ninePointsSide(
        getPlayerMatch([
          { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl },
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl },
        ])
      );
      expect(home).toBe(8);
      expect(away).toBe(12);
    });
  });
});
describe('Nine forfeit points', () => {
  const oppSl = 3;
  const sl = 3;
  test(`Home Forfeit`, () => {
    const [home, away] = ninePointsSide(
      getPlayerMatch([
        { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl, forfeit: true },
        { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl },
      ])
    );
    expect(home).toBe(0);
    expect(away).toBe(15);
  });
  test(`Away Forfeit`, () => {
    const [home, away] = ninePointsSide(
      getPlayerMatch([
        { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl },
        { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl, forfeit: true },
      ])
    );
    expect(home).toBe(15);
    expect(away).toBe(0);
  });
  test(`Home Playoff Forfeit`, () => {
    const [home, away] = ninePointsSide(
      getPlayerMatch(
        [
          { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl, forfeit: true },
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl },
        ],
        20
      )
    );
    expect(home).toBe(0);
    expect(away).toBe(20);
  });
  test(`Away Playoff Forfeit`, () => {
    const [home, away] = ninePointsSide(
      getPlayerMatch(
        [
          { raceTo: nineBallPoints(oppSl), ballPoints: nineBallPoints(oppSl) - 1, skillLevel: oppSl },
          { raceTo: nineBallPoints(sl), ballPoints: nineBallPoints(sl), skillLevel: sl, forfeit: true },
        ],
        20
      )
    );
    expect(home).toBe(20);
    expect(away).toBe(0);
  });
});
