import { TeamPoints_PlayerMatch, TeamPoint_TeamMatch } from '../types';
import { teamPoints } from '../index';
import { getPlayerMatch as eightPlayerMatch } from '../../../scoresheet/teamPoints/__tests__/lib/eightBall';
import { getPlayerMatch as ninePlayerMatch } from '../../../scoresheet/teamPoints/__tests__/lib/nineBall';
import { Side } from '../../../lib/side';
import { FormatType } from '../types';

class TeamMatch implements TeamPoint_TeamMatch {
  forfeitBy: Side;
  type: FormatType;
  matchesExpected: number = 0;
  constructor(type?: FormatType, forfeitBy?: Side, matchesExpected?: number) {
    if (type) this.type = type;
    if (forfeitBy) this.forfeitBy = forfeitBy;
    if (matchesExpected) this.matchesExpected = matchesExpected;
  }
}

const getPMList = (count: number): Array<TeamPoints_PlayerMatch> =>
  new Array(count).map(() => eightPlayerMatch([], 0, false));

describe('Error Scenarios', () => {
  test('No Input', () => {
    expect(() =>
      teamPoints({
        playerMatches: getPMList(0),
        teamFFP: 0,
        matchFFP: 0,
        teamMatch: new TeamMatch(),
        side: Side.AWAY,
      })
    ).toThrow();
  });
});

test('eightball with setup match', () => {
  const result = teamPoints({
    matchFFP: 2,
    playerMatches: getPMList(5),
    side: Side.AWAY,
    teamFFP: 8,
    teamMatch: new TeamMatch(),
  });

  expect(result).toBe(0);
});

describe('Eight Ball Team Forfeits', () => {
  describe('Home Forfeit', () => {
    test('home 0 points', () => {
      const result = teamPoints({
        matchFFP: 2,
        playerMatches: getPMList(1),
        side: Side.HOME,
        teamFFP: 8,
        teamMatch: new TeamMatch(FormatType.EIGHT, Side.HOME),
      });
      expect(result).toBe(0);
    });
    test('away 8 points', () => {
      const result = teamPoints({
        matchFFP: 2,
        playerMatches: getPMList(1),
        side: Side.AWAY,
        teamFFP: 8,
        teamMatch: new TeamMatch(FormatType.EIGHT, Side.HOME),
      });
      expect(result).toBe(8);
    });
  });
  describe('Away Forfeit', () => {
    test('home 8 points', () => {
      const result = teamPoints({
        matchFFP: 2,
        playerMatches: getPMList(1),
        side: Side.HOME,
        teamFFP: 8,
        teamMatch: new TeamMatch(FormatType.EIGHT, Side.AWAY),
      });
      expect(result).toBe(8);
    });
    test('away 0 points', () => {
      const result = teamPoints({
        matchFFP: 2,
        playerMatches: getPMList(1),
        side: Side.AWAY,
        teamFFP: 8,
        teamMatch: new TeamMatch(FormatType.EIGHT, Side.AWAY),
      });
      expect(result).toBe(0);
    });
  });
});

describe('Nine Ball Team Forfeits', () => {
  describe('Home Forfeit', () => {
    test('home 0 points', () => {
      const result = teamPoints({
        matchFFP: 15,
        playerMatches: getPMList(1),
        side: Side.HOME,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.HOME),
      });
      expect(result).toBe(0);
    });
    test('away 75 points', () => {
      const result = teamPoints({
        matchFFP: 15,
        playerMatches: getPMList(1),
        side: Side.AWAY,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.HOME),
      });
      expect(result).toBe(75);
    });
  });
  describe('Away Forfeit', () => {
    test('home 75 points', () => {
      const result = teamPoints({
        matchFFP: 15,
        playerMatches: getPMList(1),
        side: Side.HOME,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.AWAY),
      });
      expect(result).toBe(75);
    });
    test('away 0 points', () => {
      const result = teamPoints({
        matchFFP: 15,
        playerMatches: getPMList(1),
        side: Side.AWAY,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.AWAY),
      });
      expect(result).toBe(0);
    });
  });
});

describe('Nine Ball 1 match - forfeit remaining', () => {
  describe('Home Forfeit', () => {
    test('home wins match 1 12-68', () => {
      const homeResult = teamPoints({
        matchFFP: 15,
        playerMatches: [
          ninePlayerMatch([
            { ballPoints: 19, skillLevel: 2, raceTo: 19 },
            { ballPoints: 18, skillLevel: 2, raceTo: 19 },
          ]).setPlayStarted(true),
        ],
        side: Side.HOME,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.HOME, 5),
      });
      const awayResult = teamPoints({
        matchFFP: 15,
        playerMatches: [
          ninePlayerMatch([
            { ballPoints: 19, skillLevel: 2, raceTo: 19 },
            { ballPoints: 18, skillLevel: 2, raceTo: 19 },
          ]).setPlayStarted(true),
        ],
        side: Side.AWAY,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.HOME, 5),
      });

      expect(homeResult).toBe(12);
      expect(awayResult).toBe(68);
    });
  });
  describe('Away Forfeit', () => {
    test('home wins match 1 72-8', () => {
      const homeResult = teamPoints({
        matchFFP: 15,
        playerMatches: [
          ninePlayerMatch([
            { ballPoints: 19, skillLevel: 2, raceTo: 19 },
            { ballPoints: 18, skillLevel: 2, raceTo: 19 },
          ]).setPlayStarted(true),
        ],
        side: Side.HOME,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.AWAY, 5),
      });
      const awayResult = teamPoints({
        matchFFP: 15,
        playerMatches: [
          ninePlayerMatch([
            { ballPoints: 19, skillLevel: 2, raceTo: 19 },
            { ballPoints: 18, skillLevel: 2, raceTo: 19 },
          ]).setPlayStarted(true),
        ],
        side: Side.AWAY,
        teamFFP: 75,
        teamMatch: new TeamMatch(FormatType.NINE, Side.AWAY, 5),
      });

      expect(homeResult).toBe(72);
      expect(awayResult).toBe(8);
    });
  });
});
