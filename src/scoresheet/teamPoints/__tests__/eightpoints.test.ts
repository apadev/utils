import { eightPoints } from '../eightPoints';
import { getPlayerMatch } from './lib/eightBall';
import { Side } from '../../../lib/side';

import eightBallWins, { skillLevelsAvailable } from '../../../match/eightBallWins';

const eightPointsSides = (playerMatch) => [eightPoints(playerMatch, Side.HOME), eightPoints(playerMatch, Side.AWAY)];

test('eightPoints with setup match', () => {
  const result = eightPoints(getPlayerMatch([], 15), Side.AWAY);
  expect(result).toBe(0);
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`eightPoints home on hill with rack in progress ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            { raceTo: eightBallWins(sl, oppSl), wins: eightBallWins(sl, oppSl) - 1 },
            { raceTo: eightBallWins(oppSl, sl), wins: 0 },
          ],
          2,
          true
        )
      );
      expect(home).toBe(1);
      expect(away).toBe(0);
    });

    test(`eightPoints home sweep ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            { raceTo: eightBallWins(sl, oppSl), wins: eightBallWins(sl, oppSl) },
            { raceTo: eightBallWins(oppSl, sl), wins: 0 },
          ],
          2
        )
      );
      expect(home).toBe(3);
      expect(away).toBe(0);
    });
    test(`eightPoints away sweep ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            { raceTo: eightBallWins(sl, oppSl), wins: 0 },
            { raceTo: eightBallWins(oppSl, sl), wins: eightBallWins(oppSl, sl) },
          ],
          2
        )
      );
      expect(home).toBe(0);
      expect(away).toBe(3);
    });
  });
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`eightPoints hill hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            {
              raceTo: eightBallWins(sl, oppSl),
              wins: eightBallWins(sl, oppSl) - 1,
            },
            {
              raceTo: eightBallWins(oppSl, sl),
              wins: eightBallWins(oppSl, sl) - 1,
            },
          ],
          2
        )
      );
      expect(home).toBe(1);
      expect(away).toBe(1);
    });
  });
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    test(`eightPoints Home win - hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            {
              raceTo: eightBallWins(sl, oppSl),
              wins: eightBallWins(sl, oppSl),
            },
            {
              raceTo: eightBallWins(oppSl, sl),
              wins: eightBallWins(oppSl, sl) - 1,
            },
          ],
          2
        )
      );
      expect(home).toBe(2);
      expect(away).toBe(1);
    });
    test(`eightPoints away win - hill ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            {
              raceTo: eightBallWins(sl, oppSl),
              wins: eightBallWins(sl, oppSl) - 1,
            },
            {
              raceTo: eightBallWins(oppSl, sl),
              wins: eightBallWins(oppSl, sl),
            },
          ],
          2
        )
      );
      expect(home).toBe(1);
      expect(away).toBe(2);
    });
  });
});

skillLevelsAvailable.map((sl) => {
  skillLevelsAvailable.map((oppSl) => {
    // cant have 2-0 situations with race to 2. its 3/0 or 2/1
    if (eightBallWins(oppSl, sl) === 2 || eightBallWins(sl, oppSl) === 2) return;

    test(`eightPoints Home win - 1 game ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            {
              raceTo: eightBallWins(sl, oppSl),
              wins: eightBallWins(sl, oppSl),
            },
            {
              raceTo: eightBallWins(oppSl, sl),
              wins: 1,
            },
          ],
          2
        )
      );
      expect(home).toBe(2);
      expect(away).toBe(0);
    });
    test(`eightPoints away win - 1 game ${sl} vs ${oppSl}`, () => {
      const [home, away] = eightPointsSides(
        getPlayerMatch(
          [
            {
              raceTo: eightBallWins(sl, oppSl),
              wins: 1,
            },
            {
              raceTo: eightBallWins(oppSl, sl),
              wins: eightBallWins(oppSl, sl),
            },
          ],
          2
        )
      );
      expect(home).toBe(0);
      expect(away).toBe(2);
    });
  });
});
describe('Home Player Match Forfeit', () => {
  skillLevelsAvailable.map((sl) => {
    skillLevelsAvailable.map((oppSl) => {
      test(`home forfeit ${sl} vs ${oppSl}`, () => {
        const [home, away] = eightPointsSides(
          getPlayerMatch(
            [
              {
                raceTo: eightBallWins(sl, oppSl),
                wins: 0,
                forfeit: true,
              },
              {
                raceTo: eightBallWins(oppSl, sl),
                wins: 1,
              },
            ],
            2
          )
        );
        expect(home).toBe(0);
        expect(away).toBe(2);
      });
      test(`away forfeit ${sl} vs ${oppSl}`, () => {
        const [home, away] = eightPointsSides(
          getPlayerMatch(
            [
              {
                raceTo: eightBallWins(sl, oppSl),
                wins: 1,
              },
              {
                raceTo: eightBallWins(oppSl, sl),
                wins: 0,
                forfeit: true,
              },
            ],
            2
          )
        );
        expect(home).toBe(2);
        expect(away).toBe(0);
      });
    });
  });
});
