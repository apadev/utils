import { TeamPoints_PlayerMatch } from './types';
import { Side, playerOpponent } from '../../lib/side';
export const eightPoints = (playerMatch: TeamPoints_PlayerMatch, side: Side) => {
  const wins = playerMatch.racks.reduce(
    (acc, rack) => {
      if (rack.type !== 'EIGHT_RACK') return acc;
      if (rack.home.wonRack) return { home: acc.home + 1, away: acc.away };
      if (rack.away.wonRack) return { home: acc.home, away: acc.away + 1 };
      return acc;
    },
    {
      home: 0,
      away: 0,
    }
  );

  const [player, opponent] = playerOpponent(side);

  if (playerMatch[player].didForfeit || playerMatch[opponent].didForfeit)
    return playerMatch[player].didForfeit ? 0 : playerMatch.forfeitPoints;

  //match in setup if no raceTo has been set
  if (!playerMatch[player].raceTo || !playerMatch[opponent].raceTo) return 0;

  // on the hill
  if (wins[player] === playerMatch[player].raceTo - 1) return 1;
  if (wins[player] >= playerMatch[player].raceTo) {
    return wins[opponent] === 0 ? 3 : 2;
  }
  return 0;
};
