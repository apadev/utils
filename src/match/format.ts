export const matchFormatType = {
  EIGHT: 202,
  NINE: 203,
  MASTERS: 204,
};

export const formatType = (typeId: number): string => {
  switch (typeId) {
    case matchFormatType.EIGHT:
      return 'EIGHT';
    case matchFormatType.NINE:
      return 'NINE';
    case matchFormatType.MASTERS:
      return 'MASTERS';
    default:
      throw new Error(`Invalid format typeId: ${typeId}.`);
  }
};

export const formatSwitcher = (formatId: number, eight, nine, masters) => {
  if (formatId === 202) return eight;
  if (formatId === 203) return nine;
  if (formatId === 204) return masters;
};
