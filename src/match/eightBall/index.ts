export const eightBallPoints = (player, opponent, isSuddenDeath: boolean) => {
  if (opponent.didForfeit) return 3;
  // match is in setup if raceto is not set
  if (player.raceTo === 0 || opponent.raceTo === 0) return 0;

  //* Sudden Death rules
  if (isSuddenDeath) {
    if (player.racksWonNumbers.includes(1) && player.racksWonNumbers.includes(2)) {
      return 3;
    }
    if (player.racksWonNumbers.includes(1)) {
      return 2;
    }
    if (player.racksWonNumbers.includes(2)) {
      return 1;
    }
    return 0;
  }

  //* Normal rules
  if (player.racksWon === player.raceTo - 1) return 1;
  if (player.racksWon < player.raceTo) return 0;
  if (opponent.racksWon > 0) return 2;
  return 3;
};
