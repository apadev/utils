export const actualScoreEightBall = (skillLevel: number, opponentSkillLevel: number, IG: number, appliedScore: number, playerWon: boolean) => {
  if (!skillLevel) throw new Error('Missing Skill Level');
  if (!opponentSkillLevel) throw new Error('Missing Opponent Skill Level');
  if (!IG) throw new Error('Missing Innings Per Game');
  if (!appliedScore) throw new Error('Missing Applied Score');
  if (playerWon === null) throw new Error('Missing Player Won');

  if (playerWon) {
    if (skillLevel === 2) {
      if (opponentSkillLevel <= 3) {
        return 7.01
      } else {
        return 6
      }
    } else {
      return Math.min(IG, appliedScore);
    }
  } else {
    return Math.min(IG, 10);
  }
}