const EightBallSLTerm = {
  2: 10.00,
  3: 7.00,
  4: 5.00,
  5: 4.00,
  6: 3.00,
  7: 2.00,
};

export const appliedScoreEightBall = (skillLevel: number, opponentSKillLevel: number, winPercentage: number, playerWon: boolean):number => {
  if (!EightBallSLTerm[skillLevel]) throw new Error('Missing Skill Level');
  if (skillLevel === 2 && !opponentSKillLevel) throw new Error('Missing Opponent Skill Level');
  if (!winPercentage) throw new Error('Missing Win Percentage');

  const range = EightBallSLTerm[skillLevel];

  if (skillLevel === 2 && opponentSKillLevel <= 3 && playerWon) return 7.01
  if (skillLevel === 2 && opponentSKillLevel > 3 && playerWon) return 6

  return Number((range - winPercentage).toFixed(3));
}