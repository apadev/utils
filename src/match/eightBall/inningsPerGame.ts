export const inningsPerGame = (innings: number, defensiveShots: number, gamesWon: number, tableFactor: number) => {
  if (innings === null) throw new Error('Missing Innings');
  if (defensiveShots === null) throw new Error('Missing Defensive Shots');
  if (gamesWon === null) throw new Error('Missing Games Won');
  if (tableFactor === null) throw new Error('Missing Table Factor');


  if (gamesWon === 0) return 10;
  return Number((((innings - defensiveShots) / gamesWon) - tableFactor).toFixed(2));
}