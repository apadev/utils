import eightBallWins from './eightBallWins';
import nineBallPoints from './nineBallPoints';
import { formatSwitcher } from './format';

export const calculateRaceTo = (formatTypeListId: number, homeSL: number, awaySL: number) => {
  if (homeSL <= 0) {
    homeSL = 3;
  }
  if (awaySL <= 0) {
    awaySL = 3;
  }
  if (!formatTypeListId || !homeSL || !awaySL) {
    throw new Error(
      `Error calculating race to. Format Type: ${formatTypeListId}, Home SL: ${homeSL}, Away SL: ${awaySL}`
    );
  }

  const common = {
    homeSL,
    awaySL,
    format: formatTypeListId,
  };

  return formatSwitcher(
    formatTypeListId,
    {
      ...common,
      homeRaceTo: eightBallWins(homeSL, awaySL),
      awayRaceTo: eightBallWins(awaySL, homeSL),
    },
    {
      ...common,
      homeRaceTo: nineBallPoints(homeSL || 3),
      awayRaceTo: nineBallPoints(awaySL || 3),
    },
    { ...common, homeRaceTo: 7, awayRaceTo: 7 }
  );
};
