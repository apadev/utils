import { matchFormatType, formatSwitcher } from './format';

export const MatchStates = {
  SETUP: 'SETUP',
  IN_PROGRESS: 'IN_PROGRESS',
  COMPLETE: 'COMPLETE',
  COMPLETE_BY_POINTS: 'COMPLETE_BY_POINTS',
  FORFEIT: 'FORFEIT',
};

const NINE_TYPENAME = 'PlayerMatch_Side_Nine';
const EIGHT_TYPENAME = 'PlayerMatch_Side_Eight';

export const sideTypename = (format: keyof typeof matchFormatType | number) => {
  if (typeof format === 'number') return formatSwitcher(format, EIGHT_TYPENAME, NINE_TYPENAME, undefined);
  if (format === 'EIGHT') return EIGHT_TYPENAME;
  if (format === 'NINE') return NINE_TYPENAME;
};

export const state = (playerMatch: PlayerMatch_State, formatTypeListId: number) => {
  if (!playerMatch) {
    throw new Error(`No player match provided`);
  }

  const { home, away, endTime, completedByPoints, isForfeitBeforePlayStarted } = playerMatch;
  if ((home && home.didForfeit) || (away && away.didForfeit) || isForfeitBeforePlayStarted) return MatchStates.FORFEIT;
  if (completedByPoints && !endTime) return MatchStates.COMPLETE_BY_POINTS;
  if (endTime) return MatchStates.COMPLETE;
  if (!home || !away) return MatchStates.SETUP;

  if (!home.wonLag && !away.wonLag) return MatchStates.SETUP;

  if (formatTypeListId === matchFormatType.EIGHT) {
    if (home.__typename !== EIGHT_TYPENAME || away.__typename !== EIGHT_TYPENAME)
      throw new Error('type error: You must include the typename PlayerMatch_Side_Eight for eight match sides');
    // If the match is sudden death end the match after two completed racks/games
    if (playerMatch.isSuddenDeath && home.racksWon + away.racksWon >= 2) return MatchStates.COMPLETE;
    if (home.raceTo <= home.racksWon) return MatchStates.COMPLETE;
    if (away.raceTo <= away.racksWon) return MatchStates.COMPLETE;
  }
  // Nine ball is not included because hitting ball points does not === COMPLETE, end Time must be set

  return MatchStates.IN_PROGRESS;
};

export interface PlayerMatch_State {
  home: PlayerMatch_Side;
  away: PlayerMatch_Side;
  endTime?: string;
  completedByPoints?: boolean;
  isForfeitBeforePlayStarted?: boolean;
  isSuddenDeath: boolean;
}

export type PlayerMatch_Side = PlayerMatch_Side_Eight | PlayerMatch_Side_Nine;

export interface PlayerMatch_Side_Eight {
  didForfeit: boolean;
  wonLag?: boolean;
  raceTo: number;
  racksWon: number;
  __typename: 'PlayerMatch_Side_Eight';
}
export interface PlayerMatch_Side_Nine {
  didForfeit: boolean;
  wonLag?: boolean;
  raceTo: number;
  ballPoints: number;
  skillLevel: number;
  __typename: 'PlayerMatch_Side_Nine';
}
