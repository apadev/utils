export const points = {
  0: 25,
  1: 14,
  2: 19,
  3: 25,
  4: 31,
  5: 38,
  6: 46,
  7: 55,
  8: 65,
  9: 75,
};

export default (sl: number): number => points[sl];

export const skillLevelsAvailable = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
