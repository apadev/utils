import { formatSwitcher, formatType, matchFormatType } from '../index';

test('Test format switcher', () => {
  let format = formatSwitcher(matchFormatType.EIGHT, 8, 9, 'masters');
  expect(format).toBe(8);
  format = formatSwitcher(matchFormatType.NINE, 8, 9, 'masters');
  expect(format).toBe(9);
  format = formatSwitcher(matchFormatType.MASTERS, 8, 9, 'masters');
  expect(format).toBe('masters');
});

test('Test format type', () => {
  let result = formatType(matchFormatType.EIGHT);
  expect(result).toBe('EIGHT');
  result = formatType(matchFormatType.NINE);
  expect(result).toBe('NINE');
  result = formatType(matchFormatType.MASTERS);
  expect(result).toBe('MASTERS');
  expect(() => formatType(null)).toThrow(`Invalid format typeId: null.`);
});
