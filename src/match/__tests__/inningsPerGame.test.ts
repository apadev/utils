import { inningsPerGame } from '../eightBall/inningsPerGame';

test('Innings per game', () => {
  expect(inningsPerGame(20, 0, 1, .75)).toBe(19.25);
  expect(inningsPerGame(23, 3, 4, .75)).toBe(4.25);
  expect(inningsPerGame(38, 2, 4, .75)).toBe(8.25);
  expect(inningsPerGame(54, 1, 2, 0)).toBe(26.5);
  expect(inningsPerGame(54, 1, 0, 0)).toBe(10);
})

test('Innings per game error', () => {
  expect(() => {
    inningsPerGame(null, 0, 1, .75);
  }).toThrow();
  expect(() => {
    inningsPerGame(20, null, 1, .75);
  }).toThrow();
  expect(() => {
    inningsPerGame(20, 0, null, .75);
  }).toThrow();
  expect(() => {
    inningsPerGame(20, 0, 1, null);
  }).toThrow();
});