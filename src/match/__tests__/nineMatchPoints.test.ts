import { matchPointsEarned } from '../nineball/matchPointsEarned';

test('Nine match points earned', () => {
  expect(matchPointsEarned(4, 11)).toBe(2);
  expect(matchPointsEarned(3, 1)).toBe(0);
  expect(matchPointsEarned(2, 18)).toBe(8);
  expect(matchPointsEarned(2, 5)).toBe(1);
});

test('Nine match points throws a skill level error', () => {
  expect(() => {
    matchPointsEarned(null, null);
  }).toThrow();
});
