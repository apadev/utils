import { pointsPerInning } from '../nineball/pointsPerInning';

test('Points per inning', () => {
  expect(pointsPerInning(74, 29, 1, 1.09)).toBe(2.88);
  expect(pointsPerInning(74, 29, 1, 0)).toBe(2.64);
  expect(pointsPerInning(0, 29, 1, 1.09)).toBe(0);
  expect(pointsPerInning(0, 0, 1, 1.09)).toBe(0);
  expect(pointsPerInning(1, 12, 12, 1.09)).toBe(0);
});

test('Points per inning error', () => {
  expect(() => {
    pointsPerInning(null, 29, 1, 1.09);
  }).toThrow();
  expect(() => {
    pointsPerInning(74, null, 1, 1.09);
  }).toThrow();
  expect(() => {
    pointsPerInning(74, 29, null, 1.09);
  }).toThrow();
  expect(() => {
    pointsPerInning(74, 29, 1, null);
  }).toThrow();
});