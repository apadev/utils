import { state, MatchStates } from '../state';
import { playerMatch, STATE_LIB } from './lib/state.test';
import { matchFormatType } from '../format';

test('Player match state: player match missing error', () => {
  expect(() => {
    state(null, null);
  }).toThrow();
});

// SETUP
test('Player match state: returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.NO_HOME_AWAY), matchFormatType.EIGHT)).toBe(MatchStates.SETUP);
});

test('Player match state: Eight match with no home returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_NO_HOME), matchFormatType.EIGHT)).toBe(MatchStates.SETUP);
});

test('Player match state: Eight match with no away returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_NO_AWAY), matchFormatType.EIGHT)).toBe(MatchStates.SETUP);
});

test('Player match state: Nine match with no away returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.NINE_NO_AWAY), matchFormatType.NINE)).toBe(MatchStates.SETUP);
});

test('Player match state: Nine match with no home returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.NINE_NO_HOME), matchFormatType.NINE)).toBe(MatchStates.SETUP);
});

test('Player match state: Nine match with no lag winner returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.NINE_NO_LAG_WINNER), matchFormatType.NINE)).toBe(MatchStates.SETUP);
});
test('Player match state: Eight match with no lag winner returns SETUP', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_NO_LAG_WINNER), matchFormatType.EIGHT)).toBe(MatchStates.SETUP);
});

// COMPLETED
test('Player match state: Eight match returns Complete', () => {
  // TODO: Look at this to see if this scenerio is correct, should 8 be complete when racks needed are hit?
  expect(state(playerMatch(STATE_LIB.EIGHT_ENDTIME), matchFormatType.EIGHT)).toBe(MatchStates.COMPLETE);
});

test('Player match state: Nine match returns Complete', () => {
  expect(state(playerMatch(STATE_LIB.NINE_ENDTIME), matchFormatType.NINE)).toBe(MatchStates.COMPLETE);
});

// COMPLETED BY POINTS
test('Player match state: Eight match returns COMPLETE_BY_POINTS', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_COMPLETED_BY_POINTS), matchFormatType.EIGHT)).toBe(
    MatchStates.COMPLETE_BY_POINTS
  );
});
test('Player match state: Nine match returns COMPLETE_BY_POINTS', () => {
  expect(state(playerMatch(STATE_LIB.NINE_COMPLETED_BY_POINTS), matchFormatType.NINE)).toBe(
    MatchStates.COMPLETE_BY_POINTS
  );
});

// Forfeits

test('Player match state: Nine match home forfeit returns FORFEIT', () => {
  expect(state(playerMatch(STATE_LIB.NINE_HOME_FORFEIT), matchFormatType.NINE)).toBe(MatchStates.FORFEIT);
});
test('Player match state: Nine match away forfeit returns FORFEIT', () => {
  expect(state(playerMatch(STATE_LIB.NINE_AWAY_FORFEIT), matchFormatType.NINE)).toBe(MatchStates.FORFEIT);
});

test('Player match state: Eight match home forfeit returns FORFEIT', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_HOME_FORFEIT), matchFormatType.EIGHT)).toBe(MatchStates.FORFEIT);
});
test('Player match state: Eight match away forfeit returns FORFEIT', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_AWAY_FORFEIT), matchFormatType.EIGHT)).toBe(MatchStates.FORFEIT);
});

// EIGHT IN_PROGRESS
test('Player match state: Eight match with no racks won returns IN_PROGRESS', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_NO_RACKS_WON), matchFormatType.EIGHT)).toBe(MatchStates.IN_PROGRESS);
});
test('Player match state: Eight match with 1-0 returns IN_PROGRESS', () => {
  expect(state(playerMatch(STATE_LIB.EIGHT_HOME_ONE), matchFormatType.EIGHT)).toBe(MatchStates.IN_PROGRESS);
});

//NINE IN_PROGRESS
test('Player match state: Nine match with 0 ball points won returns IN_PROGRESS', () => {
  expect(state(playerMatch(STATE_LIB.NINE_ZERO_ZERO), matchFormatType.NINE)).toBe(MatchStates.IN_PROGRESS);
});
test('Player match state: Nine match with 5-0 returns IN_PROGRESS', () => {
  expect(state(playerMatch(STATE_LIB.NINE_FIVE_ZERO), matchFormatType.NINE)).toBe(MatchStates.IN_PROGRESS);
});
test('Player match state: Nine match with points reached returns IN_PROGRESS', () => {
  expect(state(playerMatch(STATE_LIB.NINE_POINTS_REACHED), matchFormatType.NINE)).toBe(MatchStates.IN_PROGRESS);
});

//Throw error for wrong player type in a match
test('Player match state: Eight match throws error with nine player passed in', () => {
  expect(() => {
    state(playerMatch(STATE_LIB.NINE_ZERO_ZERO), matchFormatType.EIGHT);
  }).toThrowError();
});
