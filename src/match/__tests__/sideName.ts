import { sideTypename } from '../state';

const NINE_TYPENAME = 'PlayerMatch_Side_Nine';
const EIGHT_TYPENAME = 'PlayerMatch_Side_Eight';

test("Player match side typenam: returns 'PlayerMatch_Side_Eight", () => {
  expect(sideTypename(202)).toBe(EIGHT_TYPENAME);
  expect(sideTypename('EIGHT')).toBe(EIGHT_TYPENAME);
});

test("Player match side typenam: returns 'PlayerMatch_Side_Nine", () => {
  expect(sideTypename(203)).toBe(NINE_TYPENAME);
  expect(sideTypename('NINE')).toBe(NINE_TYPENAME);
});

test('Player match side typenam: returns undefined (masters)', () => {
  expect(sideTypename(204)).toBe(undefined);
  expect(sideTypename('MASTERS')).toBe(undefined);
});
