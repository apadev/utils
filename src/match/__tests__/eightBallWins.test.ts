import { eightBallWins } from '../index';

test('Eight ball wins works with valid input', () => {
  let result = eightBallWins(2, 7);
  expect(result).toBe(2);
  result = eightBallWins(7, 2);
  expect(result).toBe(7);
});

test('Eight ball wins uses 3 if invalid sl used', () => {
  let result = eightBallWins(0, 7);
  expect(result).toBe(2);
  result = eightBallWins(-1, 2);
  expect(result).toBe(3);
  result = eightBallWins(-1, -2);
  expect(result).toBe(2);
});
