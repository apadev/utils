import { cappedPPI } from '../nineball/cappedPPI';

test('Capped PPI', () => {
  expect(cappedPPI(2)).toBe(3);
  expect(cappedPPI(6)).toBe(4.3);
  expect(cappedPPI(8)).toBe(5.5);
});

test('Capped PPI error', () => {
  expect(() => {
    cappedPPI(null);
  }).toThrow();
});