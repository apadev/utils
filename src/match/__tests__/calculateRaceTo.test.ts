import { match } from '../../../src/index';
import { calculateRaceTo } from '../index';

test('Testing calculate race to', () => {
  const expected = {
    homeSL: 3,
    awaySL: 4,
    format: 202,
    homeRaceTo: 2,
    awayRaceTo: 3,
  };

  const rootLevel = match.calculateRaceTo(expected.format, expected.homeSL, expected.awaySL);
  const rootMatchLevel = match.calculateRaceTo(expected.format, expected.homeSL, expected.awaySL);
  const namedLevel = calculateRaceTo(expected.format, expected.homeSL, expected.awaySL);

  expect(rootLevel).toStrictEqual(expected);
  expect(rootMatchLevel).toStrictEqual(expected);
  expect(namedLevel).toStrictEqual(expected);
});

test('Calculate race to with 8 ball sl < 2', () => {
  const input = {
    homeSL: 0,
    awaySL: -1,
    format: 202,
    homeRaceTo: 2,
    awayRaceTo: 2,
  };
  const expected = {
    homeSL: 3,
    awaySL: 3,
    format: 202,
    homeRaceTo: 2,
    awayRaceTo: 2,
  };

  const rootLevel = match.calculateRaceTo(input.format, input.homeSL, input.awaySL);
  const rootMatchLevel = match.calculateRaceTo(input.format, input.homeSL, input.awaySL);
  const namedLevel = calculateRaceTo(input.format, input.homeSL, input.awaySL);

  expect(rootLevel).toStrictEqual(expected);
  expect(rootMatchLevel).toStrictEqual(expected);
  expect(namedLevel).toStrictEqual(expected);
});

test('Calculate race fails with invalid format type id', () => {
  // @ts-expect-error Testing invalid input
  expect(() => calculateRaceTo(null, 3, 7)).toThrow(
    `Error calculating race to. Format Type: null, Home SL: 3, Away SL: 7`
  );
});
