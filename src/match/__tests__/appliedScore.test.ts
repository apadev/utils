import { appliedScoreNineBall } from '../nineball/appliedScoreNineBall';
import { appliedScoreEightBall } from '../eightBall/appliedScoreEightBall';

test('Nine ball applied score', () => {
  expect(appliedScoreNineBall(4, 4, 0.48, true)).toBe(1.94);
  expect(appliedScoreNineBall(5, 6, 0.49, false)).toBe(2.445);
  expect(appliedScoreNineBall(1, 1, 0.49, true)).toBe(1);
});

test('Nine ball applied score error', () => {
  expect(() => {
    appliedScoreNineBall(null, null, null, null);
  }).toThrow();
  expect(() => {
    appliedScoreNineBall(1, null, null, null);
  }).toThrow();
  expect(() => {
    appliedScoreNineBall(1, 2, null, null);
  }).toThrow();
});

test('Eight ball applied score', () => {
  expect(appliedScoreEightBall(6, null, 0.65, null)).toBe(2.35);
  expect(appliedScoreEightBall(5, null, 0.38, null)).toBe(3.62);
  expect(appliedScoreEightBall(2, 3, 0.38, true)).toBe(7.01);
  expect(appliedScoreEightBall(2, 4, 0.38, true)).toBe(6);
});

test('Eight ball applied score error', () => {
  expect(() => {
    appliedScoreEightBall(null, null, null, null);
  }).toThrow();
  expect(() => {
    appliedScoreEightBall(2, null, null, null);
  }).toThrow();
  expect(() => {
    appliedScoreEightBall(2, 2, null, null);
  }).toThrow();
});