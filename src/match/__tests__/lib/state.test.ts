import { PlayerMatch_State } from '../../state';
export enum STATE_LIB {
  NO_HOME_AWAY = 'noHomeAway',
  EIGHT_NO_AWAY = 'eightNoAway',
  EIGHT_NO_HOME = 'eightNoHome',
  NINE_NO_HOME = 'nineNoHome',
  NINE_NO_AWAY = 'nineNoAway',
  EIGHT_ENDTIME = 'eightEndtime',
  NINE_ENDTIME = 'nineEndtime',
  EIGHT_COMPLETED_BY_POINTS = 'eightCBP',
  NINE_COMPLETED_BY_POINTS = 'nineCBP',
  EIGHT_HOME_FORFEIT = 'eightHomeForfeit',
  EIGHT_AWAY_FORFEIT = 'eightAwayForfeit',
  NINE_HOME_FORFEIT = 'nineHomeForfeit',
  NINE_AWAY_FORFEIT = 'nineAwayForfeit',
  NINE_NO_LAG_WINNER = 'nineNoLagWinner',
  EIGHT_NO_LAG_WINNER = 'eightNoLagWinner',
  EIGHT_NO_RACKS_WON = 'eightNoRacksWon',
  EIGHT_HOME_ONE = 'eightHomeOne',
  NINE_ZERO_ZERO = 'nineZeroZero',
  NINE_FIVE_ZERO = 'nineFiveZero',
  NINE_POINTS_REACHED = 'ninePointsReached',
}

export const playerMatch = (type: STATE_LIB): PlayerMatch_State => {
  switch (type) {
    case STATE_LIB.NO_HOME_AWAY:
      return base;
    case STATE_LIB.EIGHT_NO_HOME:
      return { ...base, away: eightBase };
    case STATE_LIB.EIGHT_NO_AWAY:
      return { ...base, home: eightBase };
    case STATE_LIB.NINE_NO_HOME:
      return { ...base, away: eightBase };
    case STATE_LIB.NINE_NO_AWAY:
      return { ...base, home: eightBase };
    case STATE_LIB.EIGHT_ENDTIME:
      return {
        ...eightHomeAway,
        endTime: '2020-07-22T12:33:30-05:00',
      };
    case STATE_LIB.NINE_ENDTIME:
      return {
        ...nineHomeAway,
        endTime: '2020-07-22T12:33:30-05:00',
      };
    case STATE_LIB.EIGHT_COMPLETED_BY_POINTS:
      return {
        ...eightHomeAway,
        completedByPoints: true,
      };
    case STATE_LIB.NINE_COMPLETED_BY_POINTS:
      return {
        ...nineHomeAway,
        completedByPoints: true,
      };
    case STATE_LIB.EIGHT_HOME_FORFEIT:
      return {
        ...eightHomeAway,
        home: { ...eightBase, didForfeit: true },
      };
    case STATE_LIB.EIGHT_AWAY_FORFEIT:
      return {
        ...eightHomeAway,
        away: { ...eightBase, didForfeit: true },
      };
    case STATE_LIB.NINE_HOME_FORFEIT:
      return {
        ...eightHomeAway,
        home: { ...nineBase, didForfeit: true },
      };
    case STATE_LIB.NINE_AWAY_FORFEIT:
      return {
        ...eightHomeAway,
        away: { ...nineBase, didForfeit: true },
      };
    case STATE_LIB.EIGHT_NO_LAG_WINNER:
      return { ...eightHomeAway };
    case STATE_LIB.NINE_NO_LAG_WINNER:
      return { ...nineHomeAway };

    case STATE_LIB.EIGHT_NO_RACKS_WON:
      return { ...eightHomeLagWinner };

    case STATE_LIB.EIGHT_HOME_ONE:
      return {
        ...eightHomeLagWinner,
        home: { ...eightHomeLagWinner.home, racksWon: 1 },
      };
    case STATE_LIB.NINE_ZERO_ZERO:
      return { ...nineHomeLagWinner };
    case STATE_LIB.NINE_FIVE_ZERO:
      return {
        ...nineHomeLagWinner,
        home: { ...nineHomeLagWinner.home, ballPoints: 5 },
      };
    case STATE_LIB.NINE_POINTS_REACHED:
      return {
        ...nineHomeLagWinner,
        home: { ...nineHomeLagWinner.home, ballPoints: 14 },
      };
  }
};

const base = {
  home: null,
  away: null,
  completedByPoints: false,
  endTime: null,
};

const eightBase = {
  __typename: 'PlayerMatch_Side_Eight',
  didForfeit: false,
  raceTo: 0,
  racksWon: 0,
  wonLag: false,
};
const nineBase = {
  __typename: 'PlayerMatch_Side_Nine',
  didForfeit: false,
  raceTo: 0,
  ballPoints: 0,
  wonLag: false,
};

const eightHomeAway = { ...base, home: eightBase, away: eightBase };
const nineHomeAway = { ...base, home: nineBase, away: nineBase };
const eightHomeLagWinner = {
  ...base,
  home: { ...eightBase, wonLag: true, raceTo: 2 },
  away: { ...eightBase, raceTo: 2 },
};
const nineHomeLagWinner = {
  ...base,
  home: { ...nineBase, wonLag: true, raceTo: 14 },
  away: { ...nineBase, raceTo: 14 },
};
