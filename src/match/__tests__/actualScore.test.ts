import { actualScoreNineBall } from '../nineball/actualScoreNineBall';
import { actualScoreEightBall } from '../eightBall/actualScoreEightBall';

test('Nine ball actual score', () => {
  expect(actualScoreNineBall(5, 4.3, 3.9, true)).toBe(4.3);
  expect(actualScoreNineBall(4.13, 4.3, 3.9, true)).toBe(4.13);
  expect(actualScoreNineBall(1.72, 4.3, 3.52, true)).toBe(3.52);
  expect(actualScoreNineBall(0.6, 4.3, 0.5, true)).toBe(0.65);
  expect(actualScoreNineBall(0.35, 3, 1.48, false)).toBe(0.65);
  expect(actualScoreNineBall(1.09, 3, 1.48, false)).toBe(1.09);
  expect(actualScoreNineBall(3.1, 3, 1.48, false)).toBe(3);
});

test('Nine ball actual score error', () => {
  expect(() => {
    actualScoreNineBall(null, 3, 1.48, true);
  }).toThrow();
  expect(() => {
    actualScoreNineBall(5, null, 1.48, true);
  }).toThrow();
  expect(() => {
    actualScoreNineBall(5, 3, null, true);
  }).toThrow();
  expect(() => {
    actualScoreNineBall(5, 3, 1.48, null);
  }).toThrow();
});

test('Eight ball actual score', () => {
  expect(actualScoreEightBall(2, 3, 9, 8, true)).toBe(7.01);
  expect(actualScoreEightBall(2, 5, 9, 8, true)).toBe(6);
  expect(actualScoreEightBall(3, 5, 9, 8, true)).toBe(8);
  expect(actualScoreEightBall(3, 5, 7, 8, true)).toBe(7);
  expect(actualScoreEightBall(3, 5, 7, 8, false)).toBe(7);
  expect(actualScoreEightBall(3, 5, 11, 8, false)).toBe(10);
});

test('Eight ball actual score error', () => {
  expect(() => {
    actualScoreEightBall(null, 3, 9, 8, true);
  }).toThrow();
  expect(() => {
    actualScoreEightBall(2, null, 9, 8, true);
  }).toThrow();
  expect(() => {
    actualScoreEightBall(2, 3, null, 8, true);
  }).toThrow();
  expect(() => {
    actualScoreEightBall(2, 3, 9, null, true);
  }).toThrow();
  expect(() => {
    actualScoreEightBall(2, 3, 9, 8, null);
  }).toThrow();
});