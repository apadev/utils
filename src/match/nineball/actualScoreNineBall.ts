
export const actualScoreNineBall = (PPI: number, CPI: number, appliedScore: number, playerWon: boolean) => {
  if (!PPI) throw new Error('Missing Points Per Inning');
  if (!CPI) throw new Error('Missing Capped Points Per Inning');
  if (!appliedScore) throw new Error('Missing Applied Score');
  if (playerWon === null) throw new Error('Missing Player Won');

  if (playerWon) {
    if (PPI > appliedScore) {
      if (PPI > CPI) {
        return CPI;
      } else if (PPI < 0.65) {
        return 0.65;
      } else {
        return PPI;
      }
    } else {
      return appliedScore;
    }
  }

  if (!playerWon) {
    if (CPI < PPI) {
      return CPI;
    } else if (PPI < 0.65) {
      return 0.65;
    } else {
      return PPI;
    }
  }

}