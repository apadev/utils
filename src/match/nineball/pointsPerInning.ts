export const pointsPerInning = (matchPoints: number, innings: number, defensiveShots: number, tableFactor: number) => {
  if (matchPoints === null) throw new Error('Missing Match Point');//can be zero?
  if (innings === null) throw new Error('Missing Innings');
  if (defensiveShots === null) throw new Error('Missing Defensive Shots');
  if (tableFactor === null) throw new Error('Missing Table Factor');

  if (tableFactor === 0) tableFactor = 1;
  if (innings === 0) return 0;
  if (innings === defensiveShots) return 0;

  return Number(((tableFactor * matchPoints) / (innings - defensiveShots)).toFixed(2));
}