const PPICap = {
  1: 3,
  2: 3,
  3: 3,
  4: 3,
  5: 4.3,
  6: 4.3,
  7: 4.3,
  8: 5.5,
  9: 5.5,
}

export const cappedPPI = (skillLevel: number) => {
  if (!PPICap[skillLevel]) throw new Error('Missing Skill Level');

  return PPICap[skillLevel];
}