const NineBallSLFactorTerm = {
  1: {factor: 0.2, term: 1},
  2: {factor: 0.3, term: 1.0},
  3: {factor: 0.4, term: 1.3},
  4: {factor: 0.5, term: 1.7},
  5: {factor: 0.5, term: 2.2},
  6: {factor: 0.5, term: 2.7},
  7: {factor: 0.6, term: 3.2},
  8: {factor: 0.9, term: 3.8},
  9: {factor: 0.8, term: 4.7},
};

export const appliedScoreNineBall = (skillLevel: number, opponentSKillLevel: number, winPercentage: number, playerWon: boolean):number => {
  if (!NineBallSLFactorTerm[skillLevel]) throw new Error('Missing Skill Level');
  if (skillLevel === 1 && !opponentSKillLevel) throw new Error('Missing Opponent Skill Level');
  if (!winPercentage) throw new Error('Missing Win Percentage');

  const { factor, term } = NineBallSLFactorTerm[skillLevel];

  if (skillLevel === 1 && opponentSKillLevel === 1 && playerWon) return 1;

  return Number(((factor * winPercentage) + term).toFixed(3));
}