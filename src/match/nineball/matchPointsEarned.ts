/**
 *
 * How to use.
 *
const skillLevel = 1, ballPoints = 20;

const inProgressMatchPoints = matchPointsEarned(skillLevel, ballPoints)
const losersMatchPoints = matchPointsEarned(skillLevel, ballPoints)

const WinnerMatchPoints = 20-losersMatchPoints;
*/

export const MPEDictionary = {
  0: [5, 7, 10, 12, 15, 17, 20, 22],
  1: [3, 4, 5, 7, 8, 9, 11, 12],
  2: [4, 6, 8, 9, 11, 13, 15, 17],
  3: [5, 7, 10, 12, 15, 17, 20, 22],
  4: [6, 9, 12, 15, 19, 22, 25, 28],
  5: [7, 11, 15, 19, 23, 27, 30, 34],
  6: [9, 13, 18, 23, 28, 32, 37, 41],
  7: [11, 16, 22, 27, 33, 38, 44, 50],
  8: [14, 20, 27, 33, 40, 46, 53, 59],
  9: [18, 25, 32, 39, 47, 54, 61, 68],
};

export const matchPointsEarned = (skillLevel: number, ballPoints: number) => {
  if (!MPEDictionary[skillLevel]) {
    throw new Error('Missing Skill Level');
  }

  return MPEDictionary[skillLevel].filter((points) => points <= ballPoints).length;
};
