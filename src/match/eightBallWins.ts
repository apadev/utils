const winsNeeded = {
  0: {
    0: 2,
    2: 3,
    3: 2,
    4: 2,
    5: 2,
    6: 2,
    7: 2,
  },
  2: {
    0: 2,
    2: 2,
    3: 2,
    4: 2,
    5: 2,
    6: 2,
    7: 2,
  },
  3: {
    0: 2,
    2: 3,
    3: 2,
    4: 2,
    5: 2,
    6: 2,
    7: 2,
  },
  4: {
    0: 3,
    2: 4,
    3: 3,
    4: 3,
    5: 3,
    6: 3,
    7: 2,
  },
  5: {
    0: 4,
    2: 5,
    3: 4,
    4: 4,
    5: 4,
    6: 4,
    7: 3,
  },
  6: {
    0: 5,
    2: 6,
    3: 5,
    4: 5,
    5: 5,
    6: 5,
    7: 4,
  },
  7: {
    0: 6,
    2: 7,
    3: 6,
    4: 5,
    5: 5,
    6: 5,
    7: 5,
  },
  8: {
    0: 9,
    2: 9,
    3: 9,
    4: 9,
    5: 9,
    6: 9,
    7: 9,
  },
  9: {
    0: 9,
    2: 9,
    3: 9,
    4: 9,
    5: 9,
    6: 9,
    7: 9,
  },
};

export const eightBallWins = (yourLevel: number, opponentLevel: number): number => {
  if (!yourLevel || yourLevel < 2) {
    yourLevel = 3;
  }
  if (!opponentLevel || opponentLevel < 2) {
    opponentLevel = 3;
  }

  return winsNeeded[yourLevel][opponentLevel];
};

export default eightBallWins;

export const skillLevelsAvailable = [0, 2, 3, 4, 5, 6, 7];
